## Route Documentation
### Login

`POST /login`

Creates a session token for an user. Authenticate yourself using Basic Access Authentication.
Once you've received the token, you can authenticate yourself using JWT instead of Basic.
Send that token using the Authorization header, like this: `Authorization: JWT <token_here>`.

Header parameters:

* `Authorization` Basic. See rfc 2617.

Possible errnos:

* `10`: Username not found.
* `11`: Invalid password.

Response (string): token - A Json Web Token.

### Add account

`POST /account`

Registers a new account.

Body parameters:

* `username`(String): An username. Must be unique. No colon characters.
* `email`(String): A valid email. Must be unique.
* `password`(String): The account password.

Possible errnos:

* `1`: Username already in use.
* `2`: Email already in use.
* `3`: Invalid email syntax.
* `7`: Invalid username syntax.
* `8`: Invalid password syntax.

Response ([ResponseUser](#markdown-header-ResponseUser)): The created user.

### Update account

`PATCH /account/{id}`
Requires authentication

Updates an existing account.
The logged user must be the owner of the account or an admin.
Only an admin can edit the rank field.

Path parameters:

* `id`(Uint32): The id of the account to update.

Body parameters:

* `username`(string): A new username. Must be unique. No colon characters. Optional.
* `email`(string): A new valid email. Must be unique. Optional.
* `password`(string): A new password. Optional.
* `rank`(Uint8): A new rank (0 for user, 1 for admin). Optional.

Possible errnos:

* `1`: Username already in use.
* `2`: Email already in use.
* `3`: Invalid email syntax.
* `4`: Insufficient Permissions.
* `6`: Invalid Rank value.
* `7`: Invalid username syntax.
* `8`: Invalid password syntax.
* `9`: User Not Found.

Response ([ResponseUser](#markdown-header-ResponseUser)): The updated user.

### Delete account

`DELETE /account/{id}`
Requires authentication

Deletes an existing account.
The logged user must be the owner of the account or an admin.

Path parameters:

* `id`(Uint32): The id of the account to delete.

Possible errnos:

* `4`: Insufficient Permissions.
* `9`: User Not Found.

Response: none

### Get account

`GET /account/{id?}`

Retrieves an account.
Note: When a non-admin is retrieving an account belonging to someone else, the email will be removed.

Path parameters:

* `id`(Uint32): The id of the account to retrieve. Optional.

Possible errnos:

* `9`: User Not Found.

Response ([ResponseUser](#markdown-header-ResponseUser)): The created user.

### Get breweries

`GET /brewery`

Retrieves the list of breweries.
If lastUpdate is provided, the method will only return breweries modified after the given datetime.

Query parameters:

* `lastUpdate`(Date): a valid ISO-8601 datetime. Optional.

Possible errnos:

* `5`: Invalid datetime or date format.

Response ([Brewery](#markdown-header-Brewery)[]): No details.

### Add brewery

`POST /brewery`
Requires authentication

Adds a brewery to the list of breweries. The user must be an admin.

Body parameters:

* `name`(String): The name of the brewery.
* `body`(String): The url of the website of the brewery.
* `email`(String): The email used to contact the website.
* `phone`(String): The phone number of the website.
* `address_street`(String): The street part of the brewery's address.
* `address_num`(String): The building number part of the brewery's address.
* `address_country`(String): The country in which the brewery is located.
* `address_town`(String): The town in which the brewery is located.
* `address_postcode`(String): The postcode of the town in which the brewery is located.

Possible errnos:

* `4`: Insufficient permissions.
* `7`: Invalid brewery name.
* `17`: Invalid website syntax.
* `3`: Invalid email syntax.

Response ([Beer](#markdown-header-Beer)): The added Beer.

### Get beers

`GET /beer`

Retrieves the list of beers.
If lastUpdate is provided, the method will only return beers modified after the given datetime.

Query parameters:

* `lastUpdate`(Date): a valid ISO-8601 datetime. Optional.

Possible errnos:

* `5`: Invalid datetime or date format.

Response ([Beer](#markdown-header-Beer)[]): No details.

### Add beer

`POST /beer`
Requires authentication

Adds a beer to the list of beers. The user must be an admin.

Body parameters:

* `name`(String): The name of the beer.
* `color`(Uint32): An RGB color representing the color of the beer.
* `abv`(Float): Alcohol by volume (Float, min = 0, max = 1).
* `classification`(Uint32): The id of the classification.
* `brewery`(Uint32): The id of the brewery making the beer.

Possible errnos:

* `4`: Insufficient permissions.
* `7`: Invalid beer name.
* `12`: Invalid color.
* `14`: Invalid classification.
* `13`: Invalid ABV value.
* `15`: Invalid brewery ID.

Response ([Beer](#markdown-header-Beer)): The added Beer.

### Update beer

`PATCH /beer/{id}`
Requires authentication

Adds a beer to the list of beers. The user must be an admin.

Path parameters:

* `id`(Uint32): The id of the beer.

Body parameters:

* `name`(String): The name of the beer.
* `color`(Uint32): An RGB color representing the color of the beer.
* `abv`(Float): Alcohol by volume (Float, min = 0, max = 1).
* `classification`(Uint32): The id of the classification.
* `brewery`(Uint32): The id of the brewery making the beer.

Possible errnos:

* `4`: Insufficient permissions.
* `7`: Invalid beer name.
* `9`: No beer matched the given ID not found.
* `12`: Invalid color.
* `14`: Invalid classification.
* `13`: Invalid ABV value.
* `15`: Invalid brewery ID.

Response ([Beer](#markdown-header-Beer)): The added Beer.

### Delete beer

`DELETE /beer/{id}`
Requires authentication

Removes a beer from the list of beers. The user must be an admin.

Path parameters:

* `id`(Uint32): The id of the beer.

Possible errnos:

* `4`: Insufficient permissions.
* `9`: No beer matched the given ID not found.

Response: none

### Get beer comments

`GET /beer/{id}/comment`

Returns the list of comments posted on a beer.

Path parameters:

* `id`(Uint32): The id of the beer.

Response ([BeerComment](#markdown-header-BeerComment)[]): No details.

### Add beer comment

`POST /beer/{id}/comment`
Requires authentication

Posts a comment on a beer.

Path parameters:

* `id`(Uint32): The id of the beer.

Body parameters:

* `comment`(string): The comment to post.

Possible errnos:

* `9`: Beer not found.
* `15`: Comment format invalid.

Response ([BeerComment](#markdown-header-BeerComment)): No details.

### Edit beer comment

`PATCH /comment/{id}`
Requires authentication

Edits a comment posted on a beer. The user editing the comment must be the owner or an admin.

Path parameters:

* `id`(Uint32): The id of the beer.

Body parameters:

* `comment`(string): The replacement for the comment.

Possible errnos:

* `4`: The authenticated user cannot edit that comment.
* `9`: The comment does not exist.

Response ([BeerComment](#markdown-header-BeerComment)): No details.

### Delete beer comment

`DELETE /comment/{id}`
Requires authentication

Deletes a comment posted on a beer. The user deleting the comment must be the owner or an admin.

Path parameters:

* `id`(Uint32): The id of the beer.

Possible errnos:

* `4`: The authenticated user cannot delete that comment.
* `9`: The comment does not exist.

Response ([BeerComment](#markdown-header-BeerComment)): No details.

### Get beer user rating

`GET /beer/{beerId}/rating/user/:userId`

Returns the rating set by an user on a given beer.
Possible values are:
 -1, the user drank the beer but hasn't rated it yet.
 0 - 5, the user drank and rated the beer. The returned value is the rating.

Possible errnos:

* `9`: Not found.

Response (Uint32): the rating.

### Get beer rating

`GET /beer/{id}/rating`

Returns the beer average rating for a given beer.
Possible values are:
 0 - 5, the user drank and rated the beer. The returned value is the rating.

Possible errnos:

* `9`: Beer not found

Response (Uint32): the rating of the beer.

### Get all beer ratings

`GET /beer/rating`

Returns the list of ratings.
Possible values are:
 0 - 5, the user drank and rated the beer. The returned value is the rating.

Response (Rating): The rating of the beer.

### Set beer user rating

`PUT /beer/{beerId}/rating/user/:userId`
Requires authentication

Sets the rating of a beer for a given user. Use this if you want to mark a beer as drank too.
Possible values are:
 -1, the user drank the beer but hasn't rated it yet.
 0 - 5, the user drank and rated the beer.

Body parameters:

* `value`(Uint32): the rating.

Possible errnos:

* `4`: Unauthorised.
* `9`: Beer not found.
* `16`: Rating invalid.

Response: none

### Unmark beer as drank

`DELETE /beer/{beerId}/rating/user/:userId`
Requires authentication

Removes the rating of a beer for an user completely. Removing it from it's list of beer drank.
The authenticated user need to be an admin or the author of the rating to delete.

Possible errnos:

* `9`: Not found.
* `4`: Unauthorised.

Response: none

### Get drank beers

`GET /user/{id}/beer`
Requires authentication

Returns the list of beers a given user drank, along with their rating.
Only the actual user and admins can see the list.

Possible errnos:

* `4`: Unauthorised.

Response: none

## Type documentation

### ResponseUser

	{
	  "id": "The user's id. Uint32",
	  "username": "The user's username. String",
	  "email": "The user's email. String",
	  "rank": "The user's email. String"
	}

### Brewery

	{
	  "name": "The name of the brewery. String",
	  "body": "The url of the website of the brewery. (nullable) String",
	  "email": "The email used to contact the website. (nullable) String",
	  "phone": "The phone number of the website. (nullable) String",
	  "address_street": "The street part of the brewery's address. (nullable) String",
	  "address_num": "The building number part of the brewery's address. (nullable) String",
	  "address_country": "The country in which the brewery is located. (nullable) String",
	  "address_town": "The town in which the brewery is located. (nullable) String",
	  "address_postcode": "The postcode of the town in which the brewery is located. (nullable) String"
	}

### Beer

	{
	  "id": "The beer ID. Uint32",
	  "name": "The name of the beer. String",
	  "color": "An RGB color representing the color of the beer. Uint32",
	  "abv": "Alcohol by volume (Float, min = 0, max = 1). Float",
	  "classification": "TODO NYI. Uint32",
	  "brewery": "The brewery making the beer. Uint32",
	  "description": "The description of the beer in various languages. [BeerDescription](#markdown-header-BeerDescription)"
	}

### BeerComment

	{
	  "id": "The comment id. Uint32",
	  "text": "The comment text. string",
	  "user": "The id of the user who posted the comment. Uint32",
	  "beer": "The id of the beer on which this comment was posted. Uint32",
	  "postDate": "The date at which the comment was posted. Date",
	  "lastUpdated": "The date at which the comment was last updated. Date"
	}

### BeerDescription

	{
	  "language": "The language in which the description is written. string",
	  "description": "The description of the beer in the specified language. string"
	}
