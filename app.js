'use strict';

/** @type BelBeersMain */
const Application = require('./app/main/BelBeersMain');

module.exports = new Application();