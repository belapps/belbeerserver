'use strict';

const HttpUtils = require('../../framework/util/HttpUtils');
const FunctionUtils = require('../../framework/util/FunctionUtil');
const TypeUtils = require('../../framework/util/TypeUtils');

const Errno = require('../../framework/api/Errno');

class UserUccRouter {
	/**
	 * @typedef {Object} ResponseUser
	 * @property {!Uint32} id          The user's id.
	 * @property {!String} username    The user's username.
	 * @property {!String} email       The user's email.
	 * @property {!String} rank        The user's email.
	 */

	/**
	 * @param httpServer {!HttpServer}
	 * @param userUcc {!UserUcc}
	 */
	constructor(httpServer, userUcc) {
		this.userUcc = userUcc;

		// http://stackoverflow.com/questions/25170922/es6-how-may-one-reference-an-instance-method-bound-to-this
		FunctionUtils.bindClass(this);

		httpServer.registerRoute('POST', '/account', this.addAccount, this);
		httpServer.registerAuthRoute('PATCH', '/account/:id', this.updateAccount, this);
		httpServer.registerAuthRoute('DELETE', '/account/:id', this.deleteAccount, this);
		httpServer.registerSoftAuthRoute('GET', '/account/:id?', this.getAccount, this);
	}

	/**
	 * Registers a new account.
	 *
	 * @BodyParameter {!String} username    An username. Must be unique. No colon characters.
	 * @BodyParameter {!String} email        A valid email. Must be unique.
	 * @BodyParameter {!String} password    The account password.
	 *
	 * @Response {!ResponseUser} The created user.
	 * @Errno 1 Username already in use.
	 * @Errno 2 Email already in use.
	 * @Errno 3 Invalid email syntax.
	 * @Errno 7 Invalid username syntax.
	 * @Errno 8 Invalid password syntax.
	 *
	 * @param request An http request.
	 */
	addAccount(request) {
		if (!HttpUtils.ensureBodyParameters(request, ['username', 'email', 'password'])) {
			return;
		}

		this.userUcc.create(request.body.username, request.body.email, request.body.password, 0).then(function(user) {
			delete user.password;
			request.success(user, 201);
		}, function(error) {
			request.deny(error);
		});
	}

	/**
	 * Updates an existing account.
	 * The logged user must be the owner of the account or an admin.
	 * Only an admin can edit the rank field.
	 *
	 * @PathParameter {!Uint32} id           The id of the account to update.
	 * @BodyParameter {!string} [username]   A new username. Must be unique. No colon characters.
	 * @BodyParameter {!string} [email]      A new valid email. Must be unique.
	 * @BodyParameter {!string} [password]   A new password.
	 * @BodyParameter {!Uint8} [rank]       A new rank (0 for user, 1 for admin).
	 *
	 * @Response {!ResponseUser} The updated user.
	 * @Errno 1 Username already in use.
	 * @Errno 2 Email already in use.
	 * @Errno 3 Invalid email syntax.
	 * @Errno 4 Insufficient Permissions.
	 * @Errno 6 Invalid Rank value.
	 * @Errno 7 Invalid username syntax.
	 * @Errno 8 Invalid password syntax.
	 * @Errno 9 User Not Found.
	 *
	 * @param request An http request.
	 */
	updateAccount(request) {
		let userId = TypeUtils.parseNumber(request.params.id);
		let loggedUser = request.user;

		if (!loggedUser.isAdmin()) {
			if (userId !== loggedUser.id) {
				return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'Insufficient permissions to edit someone else\'s account.');
			}

			if (request.body.rank !== void 0) {
				return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'You are not allowed to modify the rank of an account if you are not an admin.');
			}
		}

		let rank = request.body.rank == void 0 ? void 0 : TypeUtils.parseNumber(request.body.rank);
		this.userUcc.update(userId, request.body.username, request.body.email, request.body.password, rank).then(function(user) {
			if (user === void 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Could not find an user with such ID.');
			}

			delete user.password;
			request.success(user);
		}, request.deny);
	}

	/**
	 * Deletes an existing account.
	 * The logged user must be the owner of the account or an admin.
	 *
	 * @PathParameter {!Uint32} id    The id of the account to delete.
	 * @Errno 4 Insufficient Permissions.
	 * @Errno 9 User Not Found.
	 *
	 * @param request An http request.
	 */
	deleteAccount(request) {
		let loggedUser = request.user;
		let uid = TypeUtils.parseNumber(request.params.id);

		if (uid !== loggedUser.id && !loggedUser.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'Insufficient permissions to delete someone else\'s account');
		}

		this.userUcc.destroy(uid).then(function(data) {
			if (data.length === 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Could not find an user with such ID.');
			}

			request.success();
		}, request.deny);
	}

	/**
	 * Retrieves an account.
	 * Note: When a non-admin is retrieving an account belonging to someone else, the email will be removed.
	 *
	 * @PathParameter {!Uint32=} id    The id of the account to retrieve.
	 * @Response {ResponseUser} The created user.
	 * @Errno 9 User Not Found.
	 *
	 * @param request An http request.
	 */
	getAccount(request) {
		const sendAccount = function(user) {
			if (!user) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Could not find an user with such ID.');
			}

			if (!request.user || (!request.user.isAdmin() && user.id !== request.user.id)) {
				user.email = null;
			}

			delete user.password;
			request.success(user);
		};

		if (request.params.id === void 0) {
			if (!HttpUtils.ensureLogged(request)) {
				return;
			}

			sendAccount(request.user);
		} else {
			this.userUcc.getById(TypeUtils.parseNumber(request.params.id)).then(sendAccount).catch(request.deny);
		}
	}
}

module.exports = UserUccRouter;