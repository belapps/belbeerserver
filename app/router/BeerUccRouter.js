'use strict';

const HttpUtils = require('../../framework/util/HttpUtils');
const Errno = require('../../framework/api/Errno');
const FunctionUtils = require('../../framework/util/FunctionUtil');
const TypeUtils = require('../../framework/util/TypeUtils');

class BeerUccRouter {

	/**
	 * @typedef {Object} Beer
	 * @property {!Uint32} id The beer ID.
	 * @property {!String} name The name of the beer.
	 * @property {!Uint32} color An RGB color representing the color of the beer.
	 * @property {!Float} abv Alcohol by volume (Float, min = 0, max = 1).
	 * @property {!Uint32} classification TODO NYI.
	 * @property {!Uint32} brewery The brewery making the beer.
	 * @property {!BeerDescription} description The description of the beer in various languages.
	 */

	/**
	 * @typedef {Object} BeerDescription
	 * @property {!string} language The language in which the description is written.
	 * @property {!string} description The description of the beer in the specified language.
	 */

	/**
	 * @typedef {Object} BeerComment
	 * @property {!Uint32} id The comment id.
	 * @property {!string} text The comment text.
	 * @property {!Uint32} user The id of the user who posted the comment.
	 * @property {!Uint32} beer The id of the beer on which this comment was posted.
	 * @property {!Date} postDate The date at which the comment was posted.
	 * @property {!Date} lastUpdated The date at which the comment was last updated.
	 */

	/**
	 * @param {!HttpServer} httpServer
	 * @param {!BeerUcc} beerUcc
	 * @param {!BeerCommentUcc} beerCommentUcc
	 */
	constructor(httpServer, beerUcc, beerCommentUcc) {
		this._beerUcc = beerUcc;
		this._beerCommentUcc = beerCommentUcc;

		FunctionUtils.bindClass(this);

		httpServer.registerRoute('GET', '/beer', this.getBeers, this);
		httpServer.registerAuthRoute('POST', '/beer', this.addBeer, this);
		httpServer.registerAuthRoute('PATCH', '/beer/:id', this.updateBeer, this);
		httpServer.registerAuthRoute('DELETE', '/beer/:id', this.deleteBeer, this);

		httpServer.registerRoute('GET', '/beer/:id/comment', this.getBeerComments, this);
		httpServer.registerAuthRoute('POST', '/beer/:id/comment', this.addBeerComment, this);
		httpServer.registerAuthRoute('PATCH', '/comment/:id', this.editBeerComment, this);
		httpServer.registerAuthRoute('DELETE', '/comment/:id', this.deleteBeerComment, this);

		httpServer.registerRoute('GET', '/beer/:beerId/rating/user/:userId', this.getBeerUserRating, this);
		httpServer.registerRoute('GET', '/beer/:id/rating', this.getBeerRating, this);
		httpServer.registerRoute('GET', '/beer/rating', this.getAllBeerRatings, this);
		httpServer.registerAuthRoute('PUT', '/beer/:beerId/rating/user/:userId', this.setBeerUserRating, this);
		httpServer.registerAuthRoute('DELETE', '/beer/:beerId/rating/user/:userId', this.unmarkBeerAsDrank, this);

		httpServer.registerAuthRoute('GET', '/user/:id/beer', this.getDrankBeers, this);
	}

	/**
	 * Retrieves the list of beers.
	 * If lastUpdate is provided, the method will only return beers modified after the given datetime.
	 *
	 * @QueryParameter {!Date=} lastUpdate a valid ISO-8601 datetime.
	 * @Errno 5 Invalid datetime or date format.
	 * @Response {!Beer[]}
	 * @param request An http request
	 */
	getBeers(request) {
		if (request.query.lastUpdate === void 0) {
			this._beerUcc.getAll().then(request.success).catch(request.deny);

			return;
		}

		let lastUpdateTimestamp = TypeUtils.parseTimestamp(request.query.lastUpdate);

		if (lastUpdateTimestamp === null)
			return HttpUtils.deny(request, 422, Errno.datetime_invalid, 'lastUpdate must be a valid date in ISO-8601 format');

		this._beerUcc.getModifiedSince(lastUpdateTimestamp).then(request.success).catch(request.deny);
	}

	/**
	 * Adds a beer to the list of beers. The user must be an admin.
	 *
	 * @BodyParameter {!String} name The name of the beer.
	 * @BodyParameter {!Uint32} color An RGB color representing the color of the beer.
	 * @BodyParameter {!Float} abv Alcohol by volume (Float, min = 0, max = 1).
	 * @BodyParameter {!Uint32} classification The id of the classification.
	 * @BodyParameter {!Uint32} brewery The id of the brewery making the beer.
	 * @Response {!Beer} The added Beer.
	 *
	 * @Errno 4 Insufficient permissions.
	 * @Errno 7 Invalid beer name.
	 * @Errno 12 Invalid color.
	 * @Errno 14 Invalid classification.
	 * @Errno 13 Invalid ABV value.
	 * @Errno 15 Invalid brewery ID.
	 *
	 * @param request An http request
	 */
	addBeer(request) {
		if (!request.user.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'User must be admin');
		}

		this._beerUcc.create(request.body.name, request.body.color, request.body.abv, request.body.classification, request.body.brewery)
			.then(function(data) {
				request.success(data, 201);
			}).catch(request.deny);
	}

	/**
	 * Adds a beer to the list of beers. The user must be an admin.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 * @BodyParameter {!String} name The name of the beer.
	 * @BodyParameter {!Uint32} color An RGB color representing the color of the beer.
	 * @BodyParameter {!Float} abv Alcohol by volume (Float, min = 0, max = 1).
	 * @BodyParameter {!Uint32} classification The id of the classification.
	 * @BodyParameter {!Uint32} brewery The id of the brewery making the beer.
	 * @Response {!Beer} The added Beer.
	 *
	 * @Errno 4 Insufficient permissions.
	 * @Errno 7 Invalid beer name.
	 * @Errno 9 No beer matched the given ID not found.
	 * @Errno 12 Invalid color.
	 * @Errno 14 Invalid classification.
	 * @Errno 13 Invalid ABV value.
	 * @Errno 15 Invalid brewery ID.
	 *
	 * @param request An http request
	 */
	updateBeer(request) {
		if (!request.user.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'User rank must be admin');
		}

		let id = TypeUtils.parseNumber(request.params.id);
		this._beerUcc.update(id, request.body.name, request.body.color, request.body.abv, request.body.classification, request.body.brewery)
			.then(function(beer) {
				if (beer === void 0) {
					return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Could not find a beer with such ID.');
				}

				request.success(beer);
			}).catch(request.deny);
	}

	/**
	 * Removes a beer from the list of beers. The user must be an admin.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 *
	 * @Errno 4 Insufficient permissions.
	 * @Errno 9 No beer matched the given ID not found.
	 *
	 * @param request An http request
	 */
	deleteBeer(request) {
		if (!request.user.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'User rank must be admin');
		}

		let id = TypeUtils.parseNumber(request.params.id);

		this._beerUcc.destroy(id).then(function(data) {
			if (data.length === 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Could not find an user with such ID.');
			}

			request.success();
		}).catch(request.deny);
	}

	/**
	 * Posts a comment on a beer.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 * @BodyParameter {!string} comment The comment to post.
	 *
	 * @Errno 9 Beer not found.
	 * @Errno 15 Comment format invalid.
	 *
	 * @Response {!BeerComment}
	 */
	addBeerComment(request) {
		if (!HttpUtils.ensureBodyParameters(request, ['comment']) || !HttpUtils.ensurePathParameters(request, ['id'])) {
			return;
		}

		let beerId = TypeUtils.parseNumber(request.params.id);
		let userId = request.user.id;

		this._beerCommentUcc.create(beerId, userId, request.body.comment).then(function(beer) {
			request.success(beer, 201);
		}).catch(request.deny);
	}

	/**
	 * Returns the list of comments posted on a beer.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 *
	 * @Response {!BeerComment[]}
	 */
	getBeerComments(request) {
		this._beerCommentUcc.getCommentsByBeer(request.params.id).then(request.success).catch(request.deny);
	}

	/**
	 * Edits a comment posted on a beer. The user editing the comment must be the owner or an admin.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 * @BodyParameter {!string} comment The replacement for the comment.
	 *
	 * @Errno 4 The authenticated user cannot edit that comment.
	 * @Errno 9 The comment does not exist.
	 *
	 * @Response {!BeerComment}
	 */
	editBeerComment(request) {
		let commentId = TypeUtils.parseNumber(request.params.id);
		let userId = request.user.id;

		this._beerCommentUcc.update(commentId, userId, request.body.comment).then(function(editedBeer) {
			if (editedBeer === void 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'The comment to edit does not exist.');
			}

			request.success(editedBeer);
		}).catch(request.deny);
	}

	/**
	 * Deletes a comment posted on a beer. The user deleting the comment must be the owner or an admin.
	 *
	 * @PathParameter {!Uint32} id The id of the beer.
	 *
	 * @Errno 4 The authenticated user cannot delete that comment.
	 * @Errno 9 The comment does not exist.
	 *
	 * @Response {!BeerComment}
	 */
	deleteBeerComment(request) {
		let commentId = TypeUtils.parseNumber(request.params.id);
		let userId = request.user.id;

		this._beerCommentUcc.destroySafe(commentId, userId).then(function(deletedBeers) {
			if (deletedBeers === void 0 || deletedBeers.length === 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'The comment to delete does not exist.');
			}

			request.success();
		}).catch(request.deny);
	}

	/**
	 * Sets the rating of a beer for a given user. Use this if you want to mark a beer as drank too.
	 * Possible values are:
	 *  -1, the user drank the beer but hasn't rated it yet.
	 *  0 - 5, the user drank and rated the beer.
	 *
	 * @PathParam {!Uint32} beerId the id of the beer for which we want to set the rating.
	 * @PathParam {!Uint32} userId the id of the user who rates the beer.
	 * @BodyParameter {!Uint32} value the rating.
	 *
	 * @Errno 4 Unauthorised.
	 * @Errno 9 Beer not found.
	 * @Errno 16 Rating invalid.
	 *
	 * @param request
	 */
	setBeerUserRating(request) {
		if (!HttpUtils.ensureBodyParameters(request, ['value'])) {
			return;
		}

		let userId = TypeUtils.parseNumber(request.params.userId);
		if (userId !== request.user.id) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'You cannot edit someone else\'s ratings');
		}

		let beerId = TypeUtils.parseNumber(request.params.beerId);
		let value = TypeUtils.parseNumber(request.body.value);

		this._beerUcc.setRating(beerId, userId, value).then(function(data) {
			request.success(data.rating);
		}).catch(request.deny);
	}

	/**
	 * Returns the rating set by an user on a given beer.
	 * Possible values are:
	 *  -1, the user drank the beer but hasn't rated it yet.
	 *  0 - 5, the user drank and rated the beer. The returned value is the rating.
	 *
	 * @PathParam {!Uint32} beerId the id of the beer for which we want the rating.
	 * @PathParam {!Uint32} userId the id of the user who rated the beer.
	 * @Response {!Uint32} the rating.
	 *
	 * @Errno 9 Not found.
	 *
	 * @param request
	 */
	getBeerUserRating(request) {
		let userId = TypeUtils.parseNumber(request.params.userId);
		let beerId = TypeUtils.parseNumber(request.params.beerId);

		this._beerUcc.getRating(beerId, userId).then(function(data) {
			if (data === void 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'No rating found for given user and beer.');
			}

			return request.success(data.rating);
		}).catch(request.deny);
	}

	/**
	 * Returns the beer average rating for a given beer.
	 * Possible values are:
	 *  0 - 5, the user drank and rated the beer. The returned value is the rating.
	 *
	 * @PathParam {!Uint32} id the id of the beer for which we want the rating.
	 * @Response {!Uint32} the rating of the beer.
	 *
	 * @Errno 9 Beer not found
	 *
	 * @param request
	 */
	getBeerRating(request) {
		let beerId = TypeUtils.parseNumber(request.params.id);

		this._beerUcc.getAverageRating(beerId).then(function(data) {
			if (data === void 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'No rating found for given beer.');
			}

			return request.success(data.rating);
		}).catch(request.deny);
	}

	/**
	 * Returns the list of ratings.
	 * Possible values are:
	 *  0 - 5, the user drank and rated the beer. The returned value is the rating.
	 *
	 * @PathParam {!Uint32} beerId The id of the beer for which we want the rating.
	 * @Response {!Rating} The rating of the beer.
	 *
	 * @param request
	 */
	getAllBeerRatings(request) {
		this._beerUcc.getAverageRatings().then(request.success).catch(request.deny);
	}

	/**
	 * Removes the rating of a beer for an user completely. Removing it from it's list of beer drank.
	 * The authenticated user need to be an admin or the author of the rating to delete.
	 *
	 * @PathParam {!Uint32} beerId The id of the beer for which we want to remove rating.
	 * @PathParam {!Uint32} userId The id of the user who rated the beer.
	 *
	 * @Errno 9 Not found.
	 * @Errno 4 Unauthorised.
	 *
	 * @param request
	 */
	unmarkBeerAsDrank(request) {
		let userId = TypeUtils.parseNumber(request.params.userId);
		if (userId !== request.user.id) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'You cannot edit someone else\'s beer list');
		}

		let beerId = TypeUtils.parseNumber(request.params.beerId);
		this._beerUcc.unmarkAsDrank(beerId, userId).then(function(destroyList) {
			if (destroyList.length === 0) {
				return HttpUtils.deny(request, 404, Errno.entity_not_found, 'Beer was not in the user\'s list.');
			}

			request.success();
		}).catch(request.deny);
	}

	/**
	 * Returns the list of beers a given user drank, along with their rating.
	 * Only the actual user and admins can see the list.
	 *
	 * @PathParam {!Uint32} id The id of the user we .
	 *
	 * @Errno 4 Unauthorised.
	 *
	 * @param request
	 */
	getDrankBeers(request) {
		let userId = TypeUtils.parseNumber(request.params.id);
		if (userId !== request.user.id && !request.user.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'You cannot edit someone else\'s beer list');
		}

		this._beerUcc.getDrankBeerList(userId).then(request.success).then(request.deny);
	}
}

module.exports = BeerUccRouter;