'use strict';

const HttpUtils = require('../../framework/util/HttpUtils');
const Errno = require('../../framework/api/Errno');
const FunctionUtils = require('../../framework/util/FunctionUtil');
const TypeUtils = require('../../framework/util/TypeUtils');

class BreweryUccRouter {

	/**
	 * @typedef {Object} Brewery
	 * @property {!String} name The name of the brewery.
	 * @property {String} body The url of the website of the brewery.
	 * @property {String} email The email used to contact the website.
	 * @property {String} phone The phone number of the website.
	 * @property {String} address_street The street part of the brewery's address.
	 * @property {String} address_num The building number part of the brewery's address.
	 * @property {String} address_country The country in which the brewery is located.
	 * @property {String} address_town The town in which the brewery is located.
	 * @property {String} address_postcode The postcode of the town in which the brewery is located.
	 */

	/**
	 * @param {!HttpServer} httpServer
	 * @param {!BreweryUcc} breweryUcc
	 */
	constructor(httpServer, breweryUcc) {
		this._breweryUcc = breweryUcc;

		FunctionUtils.bindClass(this);

		httpServer.registerRoute('GET', '/brewery', this.getBreweries, this);
		httpServer.registerAuthRoute('POST', '/brewery', this.addBrewery, this);
	}

	/**
	 * Retrieves the list of breweries.
	 * If lastUpdate is provided, the method will only return breweries modified after the given datetime.
	 *
	 * @QueryParameter {!Date=} lastUpdate a valid ISO-8601 datetime.
	 * @Errno 5 Invalid datetime or date format.
	 * @Response {!Brewery[]}
	 * @param request An http request
	 */
	getBreweries(request) {
		if (request.query.lastUpdate === void 0) {
			this._breweryUcc.getAll().then(request.success).catch(request.deny);

			return;
		}

		let lastUpdateTimestamp = TypeUtils.parseTimestamp(request.query.lastUpdate);

		if (lastUpdateTimestamp === null)
			return HttpUtils.deny(request, 422, Errno.datetime_invalid, 'lastUpdate must be a valid date in ISO-8601 format');

		this._breweryUcc.getModifiedSince(lastUpdateTimestamp).then(request.success).catch(request.deny);
	}

	/**
	 * Adds a brewery to the list of breweries. The user must be an admin.
	 *
	 * @BodyParameter {!String} name The name of the brewery.
	 * @BodyParameter {String} body The url of the website of the brewery.
	 * @BodyParameter {String} email The email used to contact the website.
	 * @BodyParameter {String} phone The phone number of the website.
	 * @BodyParameter {String} address_street The street part of the brewery's address.
	 * @BodyParameter {String} address_num The building number part of the brewery's address.
	 * @BodyParameter {String} address_country The country in which the brewery is located.
	 * @BodyParameter {String} address_town The town in which the brewery is located.
	 * @BodyParameter {String} address_postcode The postcode of the town in which the brewery is located.
	 * @Response {!Beer} The added Beer.
	 *
	 * @Errno 4 Insufficient permissions.
	 * @Errno 7 Invalid brewery name.
	 * @Errno 17 Invalid website syntax.
	 * @Errno 3 Invalid email syntax.
	 *
	 * @param request An http request
	 */
	addBrewery(request) {
		if (!request.user.isAdmin()) {
			return HttpUtils.deny(request, 403, Errno.insufficient_perms, 'User must be admin');
		}

		this._breweryUcc.create(request.body.name, request.body.website, request.body.email,
			request.body.phone, request.body.address_street, request.body.address_num,
			request.body.address_country, request.body.address_town, request.body.address_postcode)
			.then(function(data) {
				request.success(data, 201);
			}).catch(request.deny);
	}
}

module.exports = BreweryUccRouter;