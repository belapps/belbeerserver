'use strict';

const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const JwtStrategy = require('passport-jwt').Strategy;
const jwt = require('jsonwebtoken');

const FunctionUtils = require('../../framework/util/FunctionUtil');
const HttpUtils = require('../../framework/util/HttpUtils');
const Errno = require('../../framework/api/Errno');
const Error = require('../../framework/core/Errors');
const AUTH_TYPE = require('../../framework/api/BaseServer').AUTH_TYPE;

class Authentifier {
	/**
	 * @param {!HttpServer} httpServer.
	 * @param {!UserUcc} userUcc.
	 * @param {!Config} configuration
	 */
	constructor(httpServer, userUcc, configuration) {

		if (configuration.getProperty('jwt') == null || configuration.getProperty('jwt')['secretKey'] == null) {
			throw new Error.BaseException('jwt.secretKey not defined in configuration file.');
		}

		this._jwtSecretKey = configuration.getProperty('jwt')['secretKey'];

		FunctionUtils.bindClass(this);

		this._registerStrategies(userUcc);
		httpServer.setAuthMethod(this._isAuthenticated);
		httpServer.registerRoute('POST', '/login', this.login, this);
	}

	/**
	 * Registers the list of strategies to be used by passport.
	 * @param {!UserUcc} userUcc.
	 * @private
	 */
	_registerStrategies(userUcc) {

		// Http Basic Auth
		passport.use(new BasicStrategy(function(username, password, callback) {

			userUcc.findByUsername(username).then(function(user) {

				if (!user) {
					return callback(null, Errno.login_failed_username);
				}

				if (!user.verifyPassword(password)) {
					return callback(null, Errno.login_failed_password);
				}

				callback(null, user);
			}, callback);
		}));

		// JWT Auth
		passport.use(new JwtStrategy({ secretOrKey: this._jwtSecretKey }, function(jwtPayload, callback) {
			userUcc.getById(jwtPayload.uid).then(function(user) {

				if (!user) {
					return callback(null, Errno.login_failed_username);
				}

				callback(null, user);
			}, callback);
		}));
	}

	/**
	 * Method used for authentication when trying to access a route which requires auth.
	 * @param req The request wishing to authenticate.
	 * @param res The response to the request.
	 * @param next The callback to call once authenticated.
	 * @param {AUTH_TYPE} authType NONE: No authentication will be done.
	 *                             SOFT: Authentication is optional and the user will be authenticated if the appropriate headers are provided.
	 *                             HARD: Authentication is mandatory.
	 * @Errno 10 Invalid token.
	 * @private
	 */
	_isAuthenticated(req, res, next, authType) {
		passport.authenticate('jwt', { session: false }, function(err, user, info) {
			if (err) {
				return next(err);
			}

			if (typeof user === 'number') {
				return HttpUtils.deny(req, 401, user, 'Invalid token', {'WWW-Authenticate': 'JWT'});
			}

			if (user === false && authType === AUTH_TYPE.HARD) {
				return HttpUtils.deny(req, 401, Errno.misuse, 'Login required', {'WWW-Authenticate': 'JWT'});
			}

			req.user = user;

			next();
		})(req, res, next);
	}

	/**
	 * Creates a session token for an user. Authenticate yourself using Basic Access Authentication.
	 * Once you've received the token, you can authenticate yourself using JWT instead of Basic.
	 * Send that token using the Authorization header, like this: `Authorization: JWT <token_here>`.
	 *
	 * @HeaderParameter Authorization Basic. See rfc 2617.
	 * @Response {!string} token - A Json Web Token.
	 *
	 * @Errno 10 Username not found.
	 * @Errno 11 Invalid password.
	 *
	 * @param req The request wishing to authenticate.
	 * @param res The response to the request.
	 * @param next The callback to call once authenticated.
	 */
	login(req, res, next) {
		let self = this;

		passport.authenticate('basic', { session: false }, function(err, user, info) {
			if (err) {
				return next(err);
			}

			if (typeof user === 'number') {
				return HttpUtils.deny(req, 401, user, void 0, {'WWW-Authenticate': 'Basic realm="Belbeer"'});
			}

			if (typeof user === false) {
				return HttpUtils.deny(req, 401, Errno.misuse, 'Login required', {'WWW-Authenticate': 'Basic realm="Belbeer"'});
			}

			// make token & send.
			let token = jwt.sign({ uid: user.id }, self._jwtSecretKey);
			HttpUtils.success(req, { token: token });
		})(req, res, next);
	}
}

module.exports = Authentifier;