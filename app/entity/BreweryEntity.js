'use strict';

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) {
	return {
		connection: 'configDefinedConnection',

		identity: 'brewery',
		tableName: 'breweries',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},

			name: {
				type: 'string',
				required: true,
				notNull: true
			},

			website: {
				type: 'string',
				required: false
			},

			email: {
				type: 'email',
				required: false
			},

			phone: {
				type: 'string',
				required: false
			},

			beers: {
				collection: 'beer',
				via: 'brewery'
			},

			address_street: {
				type: 'string',
				required: false
			},

			address_num: {
				type: 'string',
				required: false
			},

			address_country: {
				type: 'string',
				required: false
			},

			address_town: {
				type: 'string',
				required: false
			},

			address_postcode: {
				type: 'string',
				required: false
			},

			lastUpdate: {
				columnName: 'last_update',
				type: 'datetime',
				required: true,
				defaultsTo: function() {
					return new Date();
				},
				index: true
			}
		},

		beforeUpdate: function(values,next) {
			values.lastUpdate = new Date();
			next();
		}
	};
};