'use strict';

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) {
	return {
		connection: 'configDefinedConnection',

		identity: 'beerDescription',
		tableName: 'beer_descriptions',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},

			beer: {
				model: 'beer',
				required: true
			},

			language: {
				type: 'string',
				required: true
			},

			description: {
				type: 'string',
				required: true
			},

			lastUpdate: {
				columnName: 'last_update',
				type: 'datetime',
				required: true,
				defaultsTo: function() {
					return new Date();
				},
				index: true
			}
		},

		beforeUpdate: function(values, callback) {
			values.lastUpdate = new Date();
			callback();
		},

		beforeValidate: function (values, callback) {
			//TODO: check there isn't 2 descriptions for the same beer in the same language
			callback();
		}
	};
};