'use strict';

const bcrypt = require('bcrypt-nodejs');
const TypeUtils = require('../../framework/util/TypeUtils');
const Errors = require('../../framework/core/Errors');
const Errno = require('../../framework/api/Errno');

const SALT_WORK_FACTOR = 10;

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) {

	return {
		connection: 'configDefinedConnection',

		identity: 'user',
		tableName: 'users',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},

			username: {
				type: 'string',
				notContains: ':',
				required: true,
				unique: true,
				notNull: true
			},

			email: {
				type: 'email',
				required: true,
				unique: true,
				notNull: true
			},

			password: {
				type: 'string',
				required: true,
				notNull: true
			},

			rank: {
				type: 'integer',
				required: true,
				notNull: true,
				'in': [0, 1],
				defaultsTo: 0
			},

			comments: {
				collection: 'beerComment',
				via: 'user'
			},

			drankBeers: {
				collection: 'beerRating',
				via: 'user'
			},

			isAdmin: function() {
				return this.rank === 1;
			},

			verifyPassword: function(password) {
				return bcrypt.compareSync(password, this.password);
			},

			changePassword: function(newPassword) {
				this.password = newPassword;
				this.rehashPassword = true;
			}
		},

		beforeCreate: function (attrs, callback) {
			bcrypt.genSalt(SALT_WORK_FACTOR, function(error, salt) {
				if (error) return callback(error);

				bcrypt.hash(attrs.password, salt, false, function(err, hash) {
					if (err) return callback(err);

					attrs.password = hash;
					return callback();
				});
			});
		},

		beforeUpdate: function (attrs, callback) {
			if (this.rehashPassword === true) {
				delete this.rehashPassword;
				return this.beforeCreate(attrs, callback);
			}

			callback();
		}
	};
};