'use strict';

const Errors = require('../../framework/core/Errors');
const Errnos = require('../../framework/api/Errno');
const TypeUtils = require('../../framework/util/TypeUtils');

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) { //call_ucc BeerUcc create beer2 0xffffff 1 1 1
	return {
		connection: 'configDefinedConnection',

		identity: 'beer',
		tableName: 'beers',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		types: {
			intColor: function(color) {
				return TypeUtils.isRGB(color);
			}
		},

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},

			name: {
				type: 'string',
				required: true,
				notNull: true
			},

			color: {
				type: 'integer',
				required: true,
				notNull: true,
				intColor: true
			},

			abv: {
				type: 'float',
				required: true,
				notNull: true,
				min: 0,
				max: 1
			},

			classification: {
				type: 'integer',
				required: true,
				notNull: true,
				min: 0
			},

			brewery: {
				model: 'brewery',
				required: true,
				notNull: true
			},

			descriptions: {
				collection: 'beerDescription',
				via: 'beer'
			},

			/*comments: {
				collection: 'beerComment',
				via: 'beer'
			},*/

			lastUpdate: {
				columnName: 'last_update',
				type: 'datetime',
				defaultsTo: function() {
					return new Date();
				},
				index: true
			}
		},

		afterValidate: function(values, next) {
			if (values.brewery === void 0) {
				return next();
			}

			dataAccessLayer.getDataAccessObject('brewery').findOne({id: values.brewery}, function(error, result) {
				if (error !== void 0) {
					next(error);
					return;
				}

				if (result === void 0) {
					next(new Errors.IllegalArgumentException('Could not find a brewery with id ' + values.brewery, Errnos.brewery_invalid, 422));
					return;
				}

				next();
			});
		},

		beforeUpdate: function(values, next) {
			values.lastUpdate = new Date();
			next();
		}
	};
};