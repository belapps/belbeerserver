'use strict';

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) {
	return {
		connection: 'configDefinedConnection',

		identity: 'beerRating',
		tableName: 'beer_ratings',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},
			//[Kev] J'ai changer 'rating' par 'value'
			value: {
				type: 'integer',
				required: false,
				notNull: false,
				min: -1,
				max: 5
			},

			user: {
				model: 'user',
				required: true,
				notNull: true,
				index: true
			},

			beer: {
				model: 'beer',
				required: true,
				notNull: true,
				index: true
			},

			lastUpdate: {
				columnName: 'last_update',
				type: 'datetime',
				defaultsTo: function() {
					return new Date();
				},
				index: true
			},

			postDate: {
				columnName: 'creation_date',
				type: 'datetime',
				defaultsTo: function() {
					return new Date();
				}
			}
		},

		beforeUpdate: function(values,next) {
			values.lastUpdate = new Date();
			next();
		},

		beforeValidate: function (attrs, callback) {
			// TODO: check there isn't 2 ratings for the same user on the same beer.
			callback();
		}
	};
};