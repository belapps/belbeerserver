'use strict';

const Errors = require('../../framework/core/Errors');
const Errnos = require('../../framework/api/Errno');

/**
 * @param {!DataAccessLayer} dataAccessLayer
 */
module.exports = function(dataAccessLayer) {
	return {
		connection: 'configDefinedConnection',

		identity: 'beerComment',
		tableName: 'beer_comments',
		autoPK: true,

		autoCreatedAt: false,
		autoUpdatedAt: false,

		attributes: {
			id: {
				type: 'integer',
				autoIncrement: true,
				primaryKey: true,
				unique: true,
				notNull: true
			},

			text: {
				type: 'string',
				required: true,
				notNull: true
			},

			user: {
				model: 'user',
				required: true,
				notNull: true
			},

			beer: {
				model: 'beer',
				required: true,
				notNull: true,
				index: true
			},

			lastUpdate: {
				columnName: 'last_update',
				type: 'datetime',
				defaultsTo: function() {
					return new Date();
				},
				index: true
			},

			postDate: {
				columnName: 'creation_date',
				type: 'datetime',
				defaultsTo: function() {
					return new Date();
				}
			}
		},

		beforeValidate: function(values, next) {
			// TODO check beer & user exist

			next();
		},

		beforeUpdate: function(values, next) {
			values.lastUpdate = new Date();
			next();
		}
	};
};