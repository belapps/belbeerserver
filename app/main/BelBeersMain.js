'use strict';

if (process.version < 'v5.0.0') {
	console.error('Requires node 5.0.0 or higher');
}

/**
 * @type {HttpServerMain}
 */
const HttpServerMain = require('../../framework/main/HttpServerMain');

class BelBeersMain extends HttpServerMain {

	setup() {
		this.addInitListener(function(app) {
			app.registerDataAccessObject('UserEntity', './app/entity/UserEntity');
			app.registerDataAccessObject('BreweryEntity', './app/entity/BreweryEntity');
			app.registerDataAccessObject('BeerEntity', './app/entity/BeerEntity');
			app.registerDataAccessObject('BeerDescriptionEntity', './app/entity/BeerDescriptionEntity');
			app.registerDataAccessObject('BeerCommentEntity', './app/entity/BeerCommentEntity');
			app.registerDataAccessObject('BeerRatingEntity', './app/entity/BeerRatingEntity');

			app.registerUseCaseController('BeerUcc', './app/ucc/BeerUcc');
			app.registerUseCaseController('UserUcc', './app/ucc/UserUcc');
			app.registerUseCaseController('BreweryUcc', './app/ucc/BreweryUcc');
			app.registerUseCaseController('BeerCommentUcc', './app/ucc/BeerCommentUcc');

			app.registerRouter('Authentifier', './app/router/Authentifier');
			app.registerRouter('BeerUccRouter', './app/router/BeerUccRouter');
			app.registerRouter('UserUccRouter', './app/router/UserUccRouter');
			app.registerRouter('BreweryUccRouter', './app/router/BreweryUccRouter');
		});

		return super.setup();
	}
}

module.exports = BelBeersMain;