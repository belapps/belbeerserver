'use strict';

const BaseUcc = require('./BaseUcc');
const Errors = require('../../framework/core/Errors');
const Errno = require('../../framework/api/Errno');
const TypeUtils = require('../../framework/util/TypeUtils');

class BeerCommentUcc extends BaseUcc {

	/**
	 * @param {!DataAccessLayer} dataAccessLayer
	 * @param {!UserUcc} userUcc
	 */
	constructor(dataAccessLayer, userUcc) {
		super(dataAccessLayer.getDataAccessObject('beerComment'));

		this._userUcc = userUcc;
	}

	/**
	 * Posts a comment on a beer.
	 *
	 * @param {!Uint32} beerId The id of the beer on which the comments is to be posted.
	 * @param {!Uint32} userId The id of the user posting the comment.
	 * @param {!string} comment The comment to post.
	 * @return {!Promise}
	 */
	create(beerId, userId, comment) {
		return this.dao.create({ beer: beerId, user: userId, text: comment });
	}

	/**
	 * Returns the list of comments posted on a beer.
	 *
	 * @param {!Uint32} beerId The id of the beer on which the comments have been posted.
	 * @return {!Promise}
	 */
	getCommentsByBeer(beerId) {
		return this.dao.find({ beer: beerId });
	}

	/**
	 * Edits a comment.
	 *
	 * @param {!Uint32} commentId The id of the comment to edit.
	 * @param {!Uint32} editorUid The id of the user editing the comment.
	 * @param {!string} newComment The new comment.
	 * @return {!Promise}
	 */
	update(commentId, editorUid, newComment) {
		return this.getById(commentId).then(function(originalComment) {
			if (originalComment === void 0) {
				return;
			}

			if (originalComment.user !== editorUid) {
				let editor = this._userUcc.getById(editorUid);

				if (!editor || !editor.isAdmin())
					throw new Errors.AuthError('Cannot edit someone else\'s comment.', Errno.insufficient_perms);
			}

			originalComment.text = newComment || originalComment.text;
			return originalComment.save();
		});
	}

	/**
	 * Deletes a comment. Checks that the editorUid is either an admin or the author of the comment.
	 *
	 * @param {!Uint32} commentId The id of the comment to delete.
	 * @param {!Uint32} editorUid The id of the user deleting the comment.
	 * @return {!Promise}
	 */
	destroySafe(commentId, editorUid) {
		let destroy = super.destroy;

		return this.getById(commentId).then(function(comment) {
			if (comment === void 0) {
				return;
			}

			if (comment.user !== editorUid) {
				let editor = this._userUcc.getById(editorUid);

				if (!editor || !editor.isAdmin())
					throw new Errors.AuthError('Cannot edit someone else\'s comment.', Errno.insufficient_perms);
			}

			return destroy(commentId);
		});
	}
}

module.exports = BeerCommentUcc;