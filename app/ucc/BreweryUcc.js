'use strict';

const FunctionUtils = require('../../framework/util/FunctionUtil');
const BaseUcc = require('./BaseUcc');

class BreweryUcc extends BaseUcc {

	/**
	 * @param {!DataAccessLayer} dataAccessLayer
	 */
	constructor(dataAccessLayer) {
		super(dataAccessLayer.getDataAccessObject('brewery'));
	}

	/**
	 * Persists a new Brewery.
	 *
	 * @param {!String} name The name of the Brewery.
	 * @param {String} website The url to the website of the brewery. Must be a valid url. Can be null.
	 * @param {String} email An email used to contact the brewery. Must be a valid email. Can be null.
	 * @param {String} phone A phone number used to contact the brewery. Can be null.
	 * @param {String} address_street The street part of the brewery's address. Can be null.
	 * @param {String} address_num The building number part of the brewery's address. Can be null.
	 * @param {String} address_country The country in which the brewery is located. Can be null.
	 * @param {String} address_town The town in which the brewery is located. Can be null.
	 * @param {String} address_postcode The postcode of the town in which the brewery is located. Can be null.
	 * @return {!Promise}
	 */
	create(name, website, email, phone, address_street, address_num, address_country, address_town, address_postcode) {
		return this.dao.create({ name, website, email, phone, address_street, address_num, address_country, address_town, address_postcode });
	}
}

module.exports = BreweryUcc;