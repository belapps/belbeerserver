'use strict';

const BaseUcc = require('./BaseUcc');
const Errors = require('../../framework/core/Errors');
const Errno = require('../../framework/api/Errno');

class BeerUcc extends BaseUcc {

	/**
	 * @param {!DataAccessLayer} dataAccessLayer
	 * @param {!UserUcc} userUcc
	 */
	constructor(dataAccessLayer, userUcc) {
		super(dataAccessLayer.getDataAccessObject('beer'));

		this._beerRatingDao = dataAccessLayer.getDataAccessObject('beerRating');
		this._userUcc = userUcc;

		let _oldGetModifiedSince = this.getModifiedSince;
		this.getModifiedSince = function(timestamp) {
			return _oldGetModifiedSince(timestamp).populate('descriptions');
		};
	}

	getAll() {
		return super.getAll().populate('descriptions');
	}

	getById(id) {
		return super.getById(id).populate('descriptions');
	}

	/**
	 * Creates a new Beer.
	 *
	 * @param {!String} name the name of the beer.
	 * @param {!Uint32} color the color of the beer.
	 * @param {!Float} abv the alcohol level of the beer.
	 * @param {!Uint32} classification the id of classification of the beer.
	 * @param {!Uint32} brewery the id of the brewery making the beer.
	 * @return {!Promise}
	 */
	create(name, color, abv, classification, brewery) {
		return this.dao.create({ name, color, abv, classification, brewery });
	}

	/**
	 * Updates an existing beer
	 *
	 * @param {!Uint32} id The id of the beer to update.
	 * @param {!String} name the name of the beer.
	 * @param {!Uint32} color the color of the beer.
	 * @param {!Float} abv the alcohol level of the beer.
	 * @param {!Uint32} classification the id of classification of the beer.
	 * @param {!Uint32} brewery the id of the brewery making the beer.
	 * @return {!Promise}
	 */
	update(id, name, color, abv, classification, brewery) {
		return super.safeGenericUpdate(id, { name, color, abv, classification, brewery });
	}

	/**
	 * Returns the average rating of a given beer.
	 *
	 * @param beerId The beer.
	 * @return {!Promise}
	 */
	getAverageRating(beerId) {
		return this._beerRatingDao.find({ beer: beerId }).average('value');
	}

	/**
	 * Returns the average rating for every beer.
	 *
	 * @return {!Promise}
	 */
	getAverageRatings() {
		return this._beerRatingDao.find().groupBy('beer').average('value');
	}

	/**
	 * Returns the rating a given user assigned to a given beer.
	 *
	 * @param beerId The rated beer.
	 * @param userId The user who rated the beer.
	 * @return {!Promise}
	 */
	getRating(beerId, userId) {
		return this._beerRatingDao.findOne({ beer: beerId, user: userId });
	}

	/**
	 * Adds or replace the rating of an user on a beer.
	 * Note: An user can only rate if he drank the beer, so a rating entry in the database automatically means the user drank the beer.
	 * If you want to mark the beer as drank but don't want to rate, set the rating value to -1.
	 *
	 * @param {!Uint32} beerId The id of the beer to rate.
	 * @param {!Uint32} userId The id of the user rating the beer.
	 * @param {!Int32} rating The rating. Value between -1 and 5 included.
	 * @return {!Promise}
	 */
	setRating(beerId, userId, rating) {
		return this._beerRatingDao.findOrCreate(
				{ beer: beerId, user: userId },
				{ beer: beerId, user: userId, rating: rating }
		).then(function(beerRating) {
			// case we created it.
			if (beerRating.value === rating) {
				return beerRating;
			}

			// case we found it and we need to update it.
			beerRating.value = rating;
			return beerRating.save();
		});
	}

	/**
	 * This will add a given beer to the list of beer drank by the given user.
	 *
	 * @param {!Uint32} beerId The id of the beer to remove from the list of beer drank by the user.
	 * @param {!Uint32} userId The id of the user removing the beer from his drank list.
	 * @return {!Promise}
	 */
	markAsDrank(beerId, userId) {
		return this._beerRatingDao.findOrCreate(
				{ beer: beerId, user: userId },
				{ beer: beerId, user: userId, value: -1 });
	}

	/**
	 * This will remove the consideration that a given beer has been drank by a given user.
	 * Note: This will also delete the rating the user gave to the beer.
	 *
	 * @param {!Uint32} beerId The id of the beer to remove from the list of beer drank by the user.
	 * @param {!Uint32} userId The id of the user removing the beer from his drank list.
	 * @return {!Promise}
	 */
	unmarkAsDrank(beerId, userId) {
		return this.dao.destroy({ beer: beerId, user: userId });
	}

	/**
	 * Returns the list of beers drank by an user.
	 * @param userId The user who drank the beers.
	 */
	getDrankBeerList(userId) {
		return this._beerRatingDao.find({ user: userId });
	}
}

module.exports = BeerUcc;