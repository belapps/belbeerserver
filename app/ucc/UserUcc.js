'use strict';

const Errors = require('../../framework/core/Errors');
const Errno = require('../../framework/api/Errno');
const BaseUcc = require('./BaseUcc');
const TypeUtils = require('../../framework/util/TypeUtils');

class UserUcc extends BaseUcc {

	constructor(dataAccessLayer) {
		super(dataAccessLayer.getDataAccessObject('user'));
	}

	/**
	 * Creates a new User.
	 * @param {!string} username The user's username.
	 * @param {!string} email The user's email.
	 * @param {!string} password The user's password.
	 * @param {!number} rank The user's rank (0 - 1).
	 * @return {!Promise} A promise containing the user.
	 */
	create(username, email, password, rank) {
		return this.dao.create({
			username,
			email,
			password,
			rank
		});
	}

	findByUsername(username) {
		if (TypeUtils.isEmptyString(username)) {
			return Promise.reject(new Errors.IllegalArgumentException('Empty username', Errno.username_invalid));
		}

		return this.dao.findOne({ username });
	}

	/**
	 * Update an User.
	 * @param {!number} id The id of the user to update.
	 * @param {!string=} username The user's username.
	 * @param {!string=} email The user's email.
	 * @param {!string=} password The user's password.
	 * @param {!number=} rank The user's rank (0 - 1).
	 * @return {!Promise} A promise containing the user.
	 */
	update(id, username, email, password, rank) {
		if (!TypeUtils.isUnsignedInt(id)) {
			return Promise.reject(new Errors.IllegalArgumentException('ID should be an integer.', Errno.entity_not_found));
		}

		return this.getById(id).then(function(user) {
			if (user === void 0) {
				return;
			}

			user.username = username !== void 0 ? username : user.username;
			user.email = email !== void 0 ? email : user.email;
			user.rank = rank !== void 0 ? rank : user.rank;

			if (password !== void 0) {
				user.changePassword(password);
			}

			return user.save();
		});
	}

	/**
	 * Sets an user as admin.
	 * @param id The user to promote.
	 * @return {Promise}
	 */
	promote(id) {
		return this.safeGenericUpdate(id, { rank: 1 });
	}
}

module.exports = UserUcc;