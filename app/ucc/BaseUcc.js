'use strict';

const FunctionUtils = require('../../framework/util/FunctionUtil');
const TypeUtils = require('../../framework/util/TypeUtils');
const Errors = require('../../framework/core/Errors');
const Errno = require('../../framework/api/Errno');

class BaseUcc {
	constructor(dao) {
		if (dao === void 0) {
			return;
		}

		if (dao === null) {
			throw new Errors.IllegalArgumentException('DAO cannot be null');
		}

		this.dao = dao;

		let self = this;

		if (dao.definition.last_update !== void 0) {
			/**
			 * Returns the list of modified entries since a given time.
			 * @param timestamp The given time.
			 *
			 * @return {!Promise}
			 * @throws IllegalArgumentException Invalid timestamp format.
			 */
			this.getModifiedSince = function(timestamp) {
				if (!TypeUtils.isUnsignedInt(timestamp)) {
					throw new Errors.IllegalArgumentException('timestamp must be a positive integer');
				}

				return self.getAll().where({ last_update: { '>': timestamp } });
			};
		}

		FunctionUtils.bindClass(this);
	}

	/**
	 * Returns every entry
	 * @return {!Promise}
	 */
	getAll() {
		return this.dao.find();
	}

	/**
	 * Returns the entry matching a given ID.
	 * @param id The given ID.
	 *
	 * @return {!Promise}
	 * @throws IllegalArgumentException Invalid id format.
	 */
	getById(id) {
		if (!TypeUtils.isUnsignedInt(id)) {
			throw new Errors.IllegalArgumentException('id must be a positive integer, got ' + id, Errno.misuse);
		}

		return this.dao.findOne({ id });
	}

	/**
	 * Destroys the entry matching a given ID.
	 * @param id The given ID.
	 *
	 * @return {!Promise}
	 * @throws IllegalArgumentException Invalid id format.
	 */
	destroy(id) {
		if (!TypeUtils.isUnsignedInt(id)) {
			throw new Errors.IllegalArgumentException('id must be a positive integer, got ' + id, Errno.misuse);
		}

		return this.dao.destroy({id});
	}

	/**
	 * Updates the given list of fields of an entity.
	 *
	 * @param id The ID of the entity to update.
	 * @param toUpdate The list of fields to update. "undefined" values are ignored when updating.
	 * @return {!Promise}
	 */
	safeGenericUpdate(id, toUpdate) {
		if (!TypeUtils.isUnsignedInt(id)) {
			return Promise.reject(new Errors.IllegalArgumentException('ID should be an integer.', Errno.entity_not_found));
		}

		for (let i in toUpdate) {
			if (toUpdate.hasOwnProperty(i) && toUpdate[i] === void 0) {
				delete toUpdate[i];
			}
		}

		return this.unsafeGenericUpdate(id, toUpdate).then(function(updatedEntities) {
			return updatedEntities.length === 0 ? void 0 : updatedEntities[0];
		});
	}

	unsafeGenericUpdate(id, toUpdate) {
		return this.dao.update({ id }, toUpdate).limit(1);
	}
}

module.exports = BaseUcc;