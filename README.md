# BelBeer? Server

### How to Install

1. Install Node 4.2.2 & NPM.
2. Clone the repository.
3. run `npm install` in the same folder as package.json.
4. run `npm run dev` or `npm start` in that folder to launch the application.

### Configuration

The configuration files are stored in the app/ folder as `config_development.json` for the development environment and `config_production.json` for the production environment.

	{
	  "database": {
	    "adapter_module": "sails-mysql",    // the database adapter (NPM module).
	    "host": "",                         // Database url/ip & port
	    "user": "",                         // Database login
	    "password": "",                     // Database password
	    "database": "belbeers"              // The database to use (must already exist)
	  }
	}

The list of adapters is available here https://github.com/balderdashy/waterline.
If using a different adapter than sails-mysql (for mysql databases), the adapter needs to be installed using `npm install <adapter_name>`. 
The configuration file might also need to be modified to match the format requested by the adapter (see their documentation).

### HTTP REST API

The API uses the HTTP protocol to communicate.  
It only accepts and produce `application/json`. (headers `Accept` and `Content-Type`)

The API will return an error code 200 if the request succeeded, and any other number if it failed (see HTTP error codes for details).  
Furthermore it will return an errno to specify the error in a way understandable by a machine.

If the `X-Human-Readable` header is set to `1`, the response will include a text message to specify the error in a way understandable by a human as well as the http error code and it's associated name.  
`X-Human-Readable: 1` should be used for development and removed (or set to `0`) while in production.

General response structure:

	{
	  "http_code": <integer>,            # if X-Human-Readable is present
	  "http_message": <string>,          # if X-Human-Readable is present
	  "error_messages": [<text>],        # human readable explanation, if X-Human-Readable is present
	  "errnos": [<integer>],             # see methods below for "errno" values
	  <success data - various>           # only in case the server needs to return something, see methods below
	  "_type": <string>                  # if X-Human-Readable is present and "success data" is an entity, this field will contain the entity type.
	}

The application should always be prepared to receiving an HTTP error code 500, which means the server encountered an unexpected error while processing the request. 

Global errno values:

* `-1`: Unknown errno value, there was a problem with the request but we don't know which one. This is a bug and should be reported. (use X-Human-Readable to debug).
* `-2`: The request was invalid because the API is being used incorrectly. This is a bug on the client's end, use X-Human-Readable to debug.
* `10`: Login failed - Invalid token, user does not exist.

#### Authenticating

Some resources are marked as requiring authentication, the authentication method is Json Web Token.
Steps on how to acquire the token are under `Login` in [API.md](API.md).

### Routes

See [API.md](API.md) for routes.