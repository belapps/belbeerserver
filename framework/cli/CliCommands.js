'use strict';

const FunctionUtil = require('../util/FunctionUtil');
const _ = require('underscore');

class CliCommands {
	constructor(appContext, logger, dependencyInjector, registry) {
		let self = this;

		this.logger = logger;
		/** @type DependencyInjector */
		this.dependencyInjector = dependencyInjector;
		/** @type AppContext */
		this.appContext = appContext;

		FunctionUtil.bindClass(this);
		this.call_ucc.USAGE = 'ucc_call <ucc_name> <method_name> [args...]';
		this.list_ucc.USAGE = 'ucc_list [ucc_name] [method_name]';

		this.helper = {
			printUsage: function (method) {
				logger.error('Usage: ' + method.USAGE);
			},

			getMethodList: function () {
				return FunctionUtil.getMethods(self);
			},

			onError: function (error) {
				if (error !== void 0) {
					self.logger.fatal('Command failed - Internal error');
					self.logger.fatal(error);
				}
			},

			getUccList: function () {
				return registry.getRegistry('ucc');
			},

			getUcc: function (uccName) {
				let uccs = this.getUccList();

				if (!_.contains(uccs, uccName)) {
					self.logger.error('Use case controller "' + uccName + '" does not exist.');
					self.logger.error('Use "ucc_list" to get the list of controllers');
					return null;
				}

				return self.dependencyInjector.create(uccName).catch(this.onError);
			},

			getUccMethod: function (uccName, methodName) {
				let ucc = this.getUcc(uccName);
				if (ucc === null) {
					return Promise.reject();
				}

				return ucc.then(function (ucc) {
					let method = ucc[methodName];
					if (method === void 0) {
						self.logger.error('Method "' + methodName + '" does not exist in UCC "' + uccName + '".');
						return Promise.reject();
					}

					return method;
				});
			}
		};
	}

	help(args) {
		this.logger.info('help <command> for details');
		this.logger.info('Available commands are: ' + this.helper.getMethodList().join(', ') + '.');
	}

	exit() {
		this.logger.info('It\'s been a pleasure.');
		this.appContext.close();
	}

	call_ucc(args) {
		if (args.length < 2) {
			this.helper.printUsage(this.call_ucc);
			this.logger.error('Use "ucc_list" for the list of available UCCs.');
			return;
		}

		let ucc = args.shift();
		let method = args.shift();

		let self = this;
		this.helper.getUccMethod(ucc, method).then(function (handler) {
			args = args.map(function (arg) {
				if (!isNaN(arg)) {
					return +arg;
				}

				if (arg === 'null') {
					return null;
				}

				if (arg === 'undefined' || arg === 'void') {
					return void 0;
				}

				try {
					return JSON.parse(arg);
				} catch (ignore) {
				}

				return arg;
			});

			self.logger.info('Calling use case controller with parameters:', args);
			let result = handler(...args);
			if (result.then !== void 0) {
				result.then(function (data) {
					self.logger.info('Request OK result:', data);
				}).catch(self.helper.onError);

				return;
			}

			self.logger.info('Request OK, result:', data);
		}).catch(this.helper.onError);
	}

	list_ucc(args) {
		if (args.length > 2) {
			return this.helper.printUsage(this.list_ucc);
		}

		if (args.length === 0) {
			this.logger.info('Available UCCs: ' + this.helper.getUccList().join(', '));
			return null;
		}

		let self = this;
		if (args.length === 1) {
			let ucc = this.helper.getUcc(args[0]);
			if (ucc === null) {
				return;
			}

			ucc.then(function (ucc) {
				let methods = FunctionUtil.getMethods(ucc);
				self.logger.info('Available methods are: ' + methods.join(', ') + '.');
			}).catch(this.helper.onError);
		}

		if (args.length === 2) {
			this.helper.getUccMethod(args[0], args[1]).then(function (method) {
				let parameters = FunctionUtil.getParameters(method);
				self.logger.info('Method: ' + args[1]);
				self.logger.info('Method Parameters: ' + parameters.join(', '));
			}).catch(this.helper.onError);
		}
	}
}

module.exports = CliCommands;