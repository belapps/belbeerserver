'use strict';

const readline = require('readline');
const CliCommands = require('./CliCommands');

class CliHandler {
	constructor(appContext, dependencyInjector, registry) {
		this.log = appContext.appLogger.child({ module: this.constructor.MODULE_NAME });
		this.commands = new CliCommands(appContext, this.log, dependencyInjector, registry);
	}

	listen() {
		if (this.closed) {
			return;
		}

		this.rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		}).on('line', this.onUserInput.bind(this));

		this.log.info('Command line interpreter listening, available commands are: ' + this.commands.helper.getMethodList().join(', '));
	}

	close() {
		if (this.rl !== void 0) {
			this.log.info('Command line interpreter closed');
			this.closed = true;
			this.rl.close();
		}
	}

	onUserInput(line) {
		line = line.trim();
		if (line === '') {
			return;
		}

		this.log.debug('Received Command', line);

		let args = line.split(' ');
		let commandName = args.shift();
		let command = this.commands[commandName];

		if (command === void 0) {
			this.log.error('Invalid command "' + commandName + '", type "help" for a list of commands');
			return;
		}

		command(args);
	}
}

module.exports = CliHandler;