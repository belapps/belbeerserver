'use strict';

const Waterline = require('waterline');
const fs = require('fs');
const _ = require('underscore');

class DataAccessLayer extends Waterline {
	/**
	 * @param {!AppContext} appContext
	 * @param {!Config} configuration
	 * @param {!Registry} registry
	 * @param {!Function} callback
	 */
	constructor(appContext, configuration, registry, callback) {
		super();

		this.logger = appContext.appLogger.child({module: this.constructor.MODULE_NAME});

		//if (appContext.envType === 'development') {
			// ensure we don't destroy the prod DB
		//	let productionConfig = configuration.getProperty('database');

		//	this._checkConfigs(productionConfig, configuration.getConfigurationForEnv('test').getProperty('database'));
		//	this._checkConfigs(productionConfig, configuration.getConfigurationForEnv('production').getProperty('database'));
		//}

		let self = this;

		let databaseConfig = configuration.getProperty('database');
		const databaseAdapter = require(databaseConfig.adapter_module);
		databaseConfig.adapter = 'configDefined';

		let config = {
			adapters: {
				'default': databaseAdapter,
				configDefined: databaseAdapter
			},

			connections: {
				configDefinedConnection: databaseConfig
			},

			defaults: {
				migrate: appContext.envType === 'test' ? 'drop' : appContext.envType === 'production' ? 'safe' : 'alter'
			}
		};

		this.loadEntities(registry.getRegistry('dao'));

		this.logger.info('Initializing Database');
		super.initialize(config, function(err, models) {
			if (err != null) {
				throw err;
			}

			self.modelList = models.collections;

			self.logger.info('Done');
			callback();
		});


	}

	_checkConfigs(productionConfig, devConfig) {
		if (devConfig.adapter === productionConfig.adapter && devConfig.host === productionConfig.host && devConfig.database === productionConfig.database) {
			throw new Error('Production and development databases are identical. This is likely an error. \n\nIn a dev environment, the database is reset every time its structure changes; production data loss will happen. \nPlease change the configuration to use a different database.');
		}
	}

	getDataAccessObject(entityName) {
		return this.modelList[entityName.toLowerCase()];
	}

	loadEntities(daoArray) {
		if (daoArray === void 0) {
			this.logger.warn('No Data Access Object registered.');
			return;
		}

		let entityFiles = daoArray;
		for (let i = 0; i < entityFiles.length; i++) {
			this.logger.info('Loading ORM entity ' + entityFiles[i]);
			let entity = require(entityFiles[i])(this);

			entity.attributes['_type'] = entity.identity;

			(function(entityType) {
				entity.attributes.getEntityType = function() {
					return entityType;
				};
			})(entity.identity);

			super.loadCollection(Waterline.Collection.extend(entity));
		}
	}

	close() {
		this.logger.info('Database Layer closing');

		let self = this;
		super.teardown(function() {
			self.logger.info('Database Layer closed');
		});
	}
}

module.exports = DataAccessLayer;