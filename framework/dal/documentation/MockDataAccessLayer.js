'use strict';

class MockDataAccessLayer {
	constructor(appContext) {
		appContext.appLogger.info('Fake database initiated');
	}

	getDataAccessObject(name) {}
}

module.exports = MockDataAccessLayer;