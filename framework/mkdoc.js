'use strict';


const path = require('path');
const baseMainFile = path.resolve(process.env.MAIN || './main/BaseMain');
const BaseMain = require(baseMainFile);

class DocGenerator extends BaseMain {

	constructor(root) {
		super(root);

		this.appContext.appLogger.info('Generating documentation for app with entry point "' + baseMainFile + '"');
	}

	defineCliOptions(optimist) {
		super.defineCliOptions(optimist);

		optimist.argv.N = true;
	}

	getInjectorConfig() {
		let config = super.getInjectorConfig();

		config.HttpServer = 'api/documentation/MockServer';
		config.DataAccessLayer = './dal/documentation/MockDataAccessLayer';
		config.RestApiDescriptor = './api/documentation/RestApiPrinter';

		return config;
	}
}

let app = new DocGenerator(path.resolve(__dirname) + '/');