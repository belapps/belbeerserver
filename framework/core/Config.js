'use strict';

class Config {

	/**
	 * @param {!AppContext} appContext
	 */
	constructor(appContext) {
		this.appContext = appContext;
	}

	_load(configFile) {
		this.config = this.appContext.requireProjectModule(configFile);
		this.appContext.appLogger.child({module: this.constructor.MODULE_NAME}).info('Loaded configuration file "' + configFile + '"');
	}

	getProperty(name) {
		if (this.config === void 0) {
			this._load('config_' + this.appContext.envType);
		}

		return this.config[name];
	}

	getConfigurationForEnv(env) {
		let newConfig = new Config(this.appContext);
		newConfig._load('config_' + env);

		return newConfig;
	}
}

module.exports = Config;