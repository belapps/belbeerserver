'use strict';

const Bunyan = require('bunyan');
const pathLib = require('path');

class AppContext {

	constructor(envType, main) {
		this._envType = envType;
		this._projectRoot = pathLib.resolve('.') + '/';
		this._frameworkRoot = pathLib.resolve(__dirname, '../') + '/';
		this._main = main;

		let info = this.require('../package.json');
		this._logger = Bunyan.createLogger({name: info.name});

		this._logger.info(info.name + ' ' + info.version + ' Starting with environment "' + envType + '" and pid ' + process.pid);
		this._logger.info(info.description);

		process.on('uncaughtException', this.onFatalError.bind(this));
	}

	/**
	 * This is for initialisation errors, do NOT use for code that runs once the app is done initialising.
	 * @param err The uncaught exception.
	 */
	onFatalError(err) {
		this._logger.fatal('Uncaught Exception');
		this._logger.fatal(err);

		this.onError(err);

		process.exit(1);
	}

	/**
	 * Call this when an error occurs in the code, it centralises error handling in case we need to email them or store them.
	 * @param error The error to handle.
	 */
	onError(error) {
		// email the error ?
	}

	/**
	 * Returns the directory in which the app is running.
	 */
	get projectRoot() {
		return this._projectRoot;
	}

	get frameworkRoot() {
		return this._frameworkRoot;
	}

	/**
	 * Returns a relative path as an absolute path with the framework root as the base path.
	 * @param path The path the make absolute.
	 */
	absolutifyPath(path) {
		if (path.startsWith('/')) return path;

		return this._frameworkRoot + path;
	}

	/**
	 * Returns a relative path as an absolute path with the project using the framework as the base path.
	 * @param path The path the make absolute.
	 */
	absolutifyProjectPath(path) {
		if (path.startsWith('/')) return path;

		return pathLib.resolve(this._projectRoot + path);
	}

	/**
	 * Like module.require but relative to the framework root.
	 * @param path the module path
	 */
	require(path) {
		return module.require(this.absolutifyPath(path));
	}

	/**
	 * Like module.require but relative to the project root.
	 * @param path the module path
	 */
	requireProjectModule(path) {
		return module.require(this.absolutifyProjectPath(path));
	}

	/**
	 * Returns the application logger.
	 */
	get appLogger() {
		return this._logger;
	}

	/**
	 * Shuts down the application.
	 */
	close() {
		this._main.close();
	}

	/**
	 * Returns a new application logger for a given module.
	 * @param module An injectable module instance.
	 */
	makeLogger(module) {
		return this.appLogger.child({module: module.constructor.MODULE_NAME});
	}

	/**
	 * Returns the environment type (ex: production, development)
	 */
	get envType() {
		return this._envType;
	}
}

module.exports = AppContext;