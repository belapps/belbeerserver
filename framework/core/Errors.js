'use strict';

class BaseException extends Error {

	constructor(message, code, httpCode) {
		super(message);
		this._code = code;
		this._httpCode = httpCode || 500;
	}

	get code() {
		return this._code;
	}

	get httpCode() {
		return this._httpCode;
	}

	toString() {
		return this.message + ': ' + this.code;
	}
}

class UserError extends BaseException {

	constructor(message, code, httpCode) {
		super(message, code, httpCode || 400);
	}
}

class AuthError extends BaseException {

	constructor(message, code, httpCode) {
		super(message || 'Insufficient permissions', code, httpCode || 403);
	}
}

class ClassNotFoundException extends BaseException {

	constructor(className, code, message) {
		super(message || 'Class %s was not found', code);
		this._className = className;
	}

	get message() {
		return util.format(super.message, this._className);
	}

	get missingClass() {
		return this._className;
	}
}

class IllegalArgumentException extends BaseException {
	constructor(message, code, httpCode) {
		super(message, code, httpCode || 422);
	}
}

module.exports.BaseException = BaseException;
module.exports.UserError = UserError;
module.exports.AuthError = AuthError;
module.exports.ClassNotFoundException = ClassNotFoundException;
module.exports.IllegalArgumentException = IllegalArgumentException;