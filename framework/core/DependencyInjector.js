'use strict';

let Errors = require('./Errors');
let FunctionUtil = require('../util/FunctionUtil');
let StringUtil = require('../util/StringUtils');

const STATE_CREATING = 0;

class DependencyInjector {

	constructor(appContext, configuration) {
		this.appContext = appContext;
		this.configuration = configuration;
		this.instanceCache = {};
		this.instanceCacheWaiters = {};

		this.logger = appContext.appLogger.child({module: 'Dependency Injector'});

		this._setCache('DependencyInjector', this);
	}

	resolveName(moduleName) {
		return this.configuration[moduleName];
	}

	getModuleClass(moduleName, fatal) {
		let clazzName = this.resolveName(moduleName);
		if (clazzName === void 0) {
			if (fatal) {
				throw new Errors.BaseException('"' + moduleName + '" is not a registered injectable module', 500);
			} else {
				return null;
			}
		}

		let clazz = this.appContext.require(clazzName);
		clazz.MODULE_NAME = moduleName;

		return clazz;
	}

	registerModule(moduleName, path) {
		if (this.configuration[moduleName] !== void 0) {
			throw new Errors.BaseException('Module "' + moduleName + '" is already registered to ' + this.configuration[moduleName], 500);
		}

		this.configuration[moduleName] = path;
	}

	create(moduleName) {
		let self = this;

		return self._getCache(moduleName).then(function(cache) {
			if (cache !== null) {
				return cache;
			}

			self.logger.info('creating instance of ' + moduleName);

			let clazz = self.getModuleClass(moduleName, true);

			return self._makeInstance(clazz).then(function(instance) {
				self.logger.info('created instance of ' + moduleName + ' (class ' + instance.constructor.name + ')');
				self._setCache(moduleName, instance);

				return instance;
			});
		}).catch(function(err) {
			self.logger.error('Error while creating ' + moduleName + ', invalidating cache');
			self._invalidateCache(moduleName);

			throw err;
		});
	}

	_getCache(moduleName) {
		let cache = this.instanceCache[moduleName];

		if (cache === void 0) {
			this.instanceCache[moduleName] = STATE_CREATING;
			return Promise.resolve(null);
		}

		// not created yet, defer the return until it is.
		if (cache === STATE_CREATING) {
			if (this.instanceCacheWaiters[moduleName] === void 0) {
				this.instanceCacheWaiters[moduleName] = [];
			}

			let self = this;
			return new Promise(function(resolve, reject) {
				self.instanceCacheWaiters[moduleName].push({resolve, reject});
			});
		}

		// already created: return the value.
		return Promise.resolve(cache);
	}

	_setCache(moduleName, instance) {
		if (this.instanceCache[moduleName] !== STATE_CREATING && this.instanceCache[moduleName] !== void 0)
			throw new Errors.IllegalArgumentException(moduleName + ' is already registered');

		this.instanceCache[moduleName] = instance;

		let waiters = this.instanceCacheWaiters[moduleName];
		if (waiters !== void 0) {
			for (let i = 0; i < waiters.length; i++) {
				waiters[i].resolve(instance);
			}

			delete this.instanceCacheWaiters[moduleName];
		}
	}

	_invalidateCache(moduleName, err) {
		delete this.instanceCache[moduleName];

		let waiters = this.instanceCacheWaiters[moduleName];
		if (waiters !== void 0) {
			for (let i = 0; i < waiters.length; i++) {
				waiters[i].reject(err);
			}

			delete this.instanceCacheWaiters[moduleName];
		}
	}

	_makeInstance(clazz) {
		let self = this;

		return new Promise(function(resolve, reject) {
			let parameters = FunctionUtil.getParameters(clazz);
			let parameterInstances = [];
			let createdParameterCount = 0;
			let callbackCount = 0;
			let moduleInstance;

			function instantiate() {
				let instantResolve = callbackCount === 0;

				try {
					moduleInstance = new clazz(...parameterInstances);
				} catch(e) {
					reject(e);
				}


				if (instantResolve) {
					resolve(moduleInstance);
				}
			}

			self.logger.debug('Requested class constructor has ' + parameters.length + ' parameter(s): ' + parameters);

			if (parameters.length === 0) {
				instantiate();
			}

			for (let i = 0; i < parameters.length; i++) {
				if (parameters[i] === 'callback') {
					callbackCount++;
					createdParameterCount++;

					parameterInstances[i] = function() {
						if (--callbackCount === 0) {
							if (moduleInstance === void 0) {
								throw new Errors.BaseException('Class ' + clazz.name + ' is requesting a constructor callback but calls it before the class finishes constructing.');
							}

							resolve(moduleInstance);
						}
					};
				} else {
					(function(parameterNum) {
						let capitalizedName = StringUtil.capitalize(parameters[parameterNum]);

						self.create(capitalizedName).then(function(parameterInstance) {
							createdParameterCount++;
							parameterInstances[parameterNum] = parameterInstance;

							if (createdParameterCount === parameters.length) {
								instantiate();
							}
						}, reject);
					})(i);
				}
			}
		});
	}
}

module.exports = DependencyInjector;