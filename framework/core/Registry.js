'use strict';

class Registry {
	constructor() {
		this._registries = {};
	}

	getRegistry(name) {
		return this._registries[name];
	}

	addValue(registryName, value) {
		if (this._registries[registryName] === void 0) {
			this._registries[registryName] = [];
		}

		this._registries[registryName].push(value);
	}
}

module.exports = Registry;