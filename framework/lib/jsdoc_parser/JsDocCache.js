'use strict';

const Errors = require('../../core/Errors');
const parser = require('./JsDocParser');

const IS_PARSING = 12345;

class JsDocCache {

	// TODO: cleanly purge this ?
	constructor(dependencyInjector, appContext) {
		this._dependencyInjector = dependencyInjector;
		this._appContext = appContext;

		this._typedefs = {};
		this._moduleCache = {};

		/** @type {Object.<string, { callback: function, functionName: string }[]>} */
		this._waitingCallbacks = {};
	}

	/**
	 * @callback parseCallback
	 * @param {!Object} jsdoc
	 */

	/**
	 * Returns the documentation of a function in a given module.
	 * @param {!string} moduleName The name of the injectable module to parse.
	 * @param {!string} functionName The name of the method in the module.
	 * @param {!parseCallback} callback The function to call once the JSDoc has been parsed.
	 */
	getDocumentation(moduleName, functionName, callback) {
		if (this._getCache(moduleName, functionName, callback)) {
			return;
		}

		let path = this._dependencyInjector.resolveName(moduleName);

		if (path === void 0) {
			throw new Errors.IllegalArgumentException(moduleName + ' is not a registered module');
		}

		let self = this;
		let modulePath = this._appContext.absolutifyPath(path) + '.js';
		parser(['-c', __dirname + '/customtags.json', modulePath], function(error, data) {
			if (error) {
				callback(error, null);
			}

			self._processAndCacheDoc(moduleName, data);
		});
	}

	/**
	 * Returns a type defined using @typedef.
	 * @param typeName
	 */
	getTypeDefinition(typeName) {
		return this._typedefs[typeName];
	}

	_processAndCacheDoc(moduleName, dataBundle) {
		let moduleCache = {};

		for (let i = 0; i < dataBundle.length; i++) {
			let methodDoc = dataBundle[i];

			switch (methodDoc.kind) {
				case 'typedef':
					this._typedefs[methodDoc.name] = methodDoc;
					break;
				case 'function':
					moduleCache[methodDoc.name] = methodDoc;
					break;
			}
		}

		this._cache(moduleName, moduleCache);
	}

	/**
	 * Tries fetching the documentation in the doc cache. If the documentation is not found, the callback will be
	 * added to a waiting list and called once {@link _cache} is called with the documentation.
	 *
	 * @private
	 *
	 * @param moduleName                    The name of the module containing the function
	 * @param functionName                  The name of the function
	 * @param {parseCallback} callback      The method to call once the data is available
	 * @return {boolean}                    false: the data should be built. true: it is being build/is built
	 */
	_getCache(moduleName, functionName, callback) {
		let cache = this._moduleCache[moduleName];
		if (cache === IS_PARSING) {
			this._waitingCallbacks[moduleName].push({functionName, callback});
			return true;
		}

		if (cache !== void 0) {
			callback(null, cache[functionName]);
			return true;
		}

		this._moduleCache[moduleName] = IS_PARSING;
		this._waitingCallbacks[moduleName] = [{functionName, callback}];

		return false;
	}

	_cache(moduleName, moduleCache) {
		this._moduleCache[moduleName] = moduleCache;

		let callbacks = this._waitingCallbacks[moduleName];
		if (callbacks === void 0) return;

		for (let i = 0; i < callbacks.length; i++) {
			let callbackData = callbacks[i];
			let methodCache = moduleCache[callbackData.functionName];
			callbackData.callback(null, methodCache);
		}
	}
}

module.exports = JsDocCache;