'use strict';

const walkback = require('walk-back');
const cprocess = require('child_process');
const path = require('path');

module.exports = function(args, done) {
	let jsdocPath = walkback(__dirname, path.join('node_modules', 'jsdoc-75lb', 'jsdoc.js'));
	var jsdocTemplatePath = __dirname;

	if (!jsdocPath) {
		throw new Error('jsdoc-parse: cannot find jsdoc');
	}

	args.unshift('node', jsdocPath, '--pedantic', '-t', jsdocTemplatePath);

	cprocess.exec(args.join(' '), function(error, stdout, stderr) {
		done(error, stdout !== '' ? JSON.parse(stdout) : null);
	});
};