'use strict';

exports.defineTags = function(dictionary) {
	dictionary.defineTag('bodyparameter', {
		canHaveType: true,
		canHaveName: true,
		onTagged: parseParameter
	});

	dictionary.defineTag('pathparameter', {
		canHaveType: true,
		canHaveName: true,
		onTagged: parseParameter
	});

	dictionary.defineTag('headerparameter', {
		canHaveType: true,
		canHaveName: true,
		onTagged: parseParameter
	});

	dictionary.defineTag('queryparameter', {
		canHaveType: true,
		canHaveName: true,
		onTagged: parseParameter
	});

	dictionary.defineTag('errno', {
		canHaveType: false,
		canHaveName: false,
		onTagged: function(doclet, tag) {
			doclet.errnos = doclet.errnos || [];

			let error_number = tag.text.match(/^([\d]*)/gm);
			let error_message = tag.text.match(/^[\d]*[\s]*(.+)/m);

			sanitize(tag);

			tag.num = error_number[0] >> 0;
			tag.message = error_message[1];

			doclet.errnos.push(tag);
		}
	});

	dictionary.defineTag('response', {
		canHaveType: true,
		canHaveName: false,
		onTagged: function(doclet, tag) {
			sanitize(tag);

			doclet.response = tag;
		}
	});
};

function parseParameter(doclet, tag) {
	if (doclet.params === void 0) {
		doclet.params = [];
	}

	tag.paramType = tag.title;
	sanitize(tag);

	doclet.params.push(tag);
}

function sanitize(tag) {
	if (typeof tag.value === 'object')
		merge(tag, tag.value);

	if (typeof tag.type === 'object')
		tag.type = tag.type.names;

	delete tag.value;
	delete tag.originalTitle;
	delete tag.title;
	delete tag.text;
}

function merge(obj1, obj2) {
	for (let i in obj2) {
		obj1[i] = obj2[i];
	}
}