'use strict';

const array_tools = require('array-tools');

exports.publish = function(data) {

	let json = array_tools.where(data().get(), {
		'!undocumented': true, '!kind': /package|file/
	});

	console.log(JSON.stringify(json, null, '  '));
};
