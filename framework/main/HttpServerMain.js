'use strict';

const AppContext = require('../core/AppContext');
const DependencyInjector = require('../core/DependencyInjector');
const path = require('path');
const optimist = require('optimist');
const BaseMain = require('./BaseMain');

/**
 * Entry point for the application. Initializes a Web server.
 */
class HttpServerMain extends BaseMain {

	defineCliOptions(optimist) {
		super.defineCliOptions(optimist);
		optimist.alias('P', 'port').describe('P', 'Defines the HTTP Server port.').default('P', 3000);
	}

	getInjectorConfig() {
		let config = super.getInjectorConfig();

		config.RestApiDescriptor = './api/http/RestApiDescriptor';
		config.HttpServer = './api/http/HttpServer';

		return config;
	}

	/**
	 * Registers a Router, it will be instantiated when the web server launches.
	 * @param name The name of the Router.
	 * @param path The path to the file containing the Router class. Either absolute or from the node entry point.
	 */
	registerRouter(name, path) {
		this.appContext.appLogger.info('Registering Router', name);
		this._registerCustomModule(name, path);
		this._routers.push(name);
	}

	setup() {
		this._routers = [];

		let self = this;
		this.addPostInitListener(function() {
			return self._createHttpServer();
		});

		this.addCloseListener(function() {
			self._server.close();
		});

		return super.setup();
	}

	_createHttpServer() {
		let injector = this.injector;
		let self = this;

		return injector.create('HttpServer').then(function(server) {
			self._server = server;

			let routerNames = self._routers;
			let routerInstances = [];

			for (let router of routerNames) {
				routerInstances.push(injector.create(router));
			}

			return Promise.all(routerInstances).then(function() {
				server.listen(optimist.argv.P || 3000);
			});
		}).catch(function(err) {
			self.appContext.onFatalError(err);
		});
	}
}

module.exports = HttpServerMain;