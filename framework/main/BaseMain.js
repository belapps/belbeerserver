'use strict';

const AppContext = require('../core/AppContext');
const DependencyInjector = require('../core/DependencyInjector');
const path = require('path');
const optimist = require('optimist');

/**
 * Application entry point.
 */
class BaseMain {

	constructor() {
		this._state = BaseMain.STATE.BOOT;

		if (process.env.NODE_ENV === void 0) {
			process.env.NODE_ENV = 'production';
		}

		this._listeners = { preinit: [], init: [], postinit: [], closing: [], ready: [] };
		this.appContext = new AppContext(process.env.NODE_ENV, this); // TODO: merge appcontext & main ?

		this.setup();

		let self = this;
		this._state = BaseMain.STATE.PRE_INIT;
		this._dispatchStateChangeEvent('preinit').then(function() {
			self._state = BaseMain.STATE.INIT;
			return self._dispatchStateChangeEvent('init');
		}).then(function() {
			self._state = BaseMain.STATE.POST_INIT;
			return self._dispatchStateChangeEvent('postinit');
		}).then(function() {
			self._state = BaseMain.STATE.READY;
			return self._dispatchStateChangeEvent('ready');
		}).catch(function(error) {
			self.appContext.onFatalError(error);
		});
	}

	defineCliOptions(optimist) {
		optimist.boolean('H').alias('H', 'help').describe('H', 'Prints help.');
		optimist.alias('P', 'port').describe('P', 'Defines the HTTP Server port.').default('P', 3000);
		optimist.boolean('N').alias('N', 'no-cli').describe('N', 'Disables the CLI admin panel.');
	}

	getCliOption(shortName) {
		return optimist.argv[shortName];
	}

	/**
	 * Gets the dependency injector configuration, defines what implementation the modules use.
	 * @return {!Object.<string, string>}
	 */
	getInjectorConfig() {
		return {
			DataAccessLayer: './dal/waterline/DataAccessLayer',
			Configuration: './core/Config',
			JsDocParser: './lib/jsdoc_parser/JsDocCache',
			CliHandler: './cli/CliHandler',
			Registry: './core/Registry'
		};
	}

	_registerCustomModule(name, path) {
		this._injector.registerModule(name, this.appContext.absolutifyProjectPath(path));
	}

	/**
	 * Registers a Use Case Controller, it will be made available through injection.
	 * @param name The name of the UCC.
	 * @param path The path to the file containing the UCC class. Either absolute or from the node entry point.
	 */
	registerUseCaseController(name, path) {
		this.appContext.appLogger.info('Registering UCC', name);
		this._registerCustomModule(name, path);
		this.registry.addValue('ucc', name);
	}

	/**
	 * Registers a (waterline) entity, it's dao will be available through an injected {@link DataAccessLayer}.
	 * @param name The name of the entity.
	 * @param path The path to the file containing the entity. Either absolute or from the node entry point.
	 */
	registerDataAccessObject(name, path) {
		this.appContext.appLogger.info('Registering DAO', name);
		this.registry.addValue('dao', this.appContext.absolutifyProjectPath(path));
	}

	/**
	 * Initializes the application, creates it's components and registers the state change handlers.
	 */
	setup() {
		let self = this;

		this.defineCliOptions(optimist);
		if (optimist.argv.H) {
			this.appContext.appLogger.info(optimist.help());
			return;
		}

		this._injector = new DependencyInjector(this.appContext, this.getInjectorConfig());
		this._injector._setCache('AppContext', this.appContext);

		this._addPreInitListener(function() {
			return self._injector.create('Registry').then(function(registry) {
				self.registry = registry;
			}, self.appContext.onFatalError);
		});

		this.addPostInitListener(function() {
			if (optimist.argv.N) {
				self.appContext.appLogger.warn('CLI panel disabled');
			} else {
				return self._injector.create('CliHandler').then(function(handler) {
					handler.listen();
				}).catch(self.appContext.onFatalError);
			}
		});

		this.addCloseListener(function() {
			if (!optimist.argv.N) {
				return self._injector.create('CliHandler').then(function(handler) {
					handler.close();
				});
			}
		});

		this.addCloseListener(function() {
			return self._injector.create('DataAccessLayer').then(function(dal) {
				dal.close();
			});
		});
	}

	get state() {
		return this._state;
	}

	/**
	 * Returns the app Dependency Injector.
	 * @return {DependencyInjector}
	 */
	get injector() {
		return this._injector;
	}

	/**
	 * Shuts down the application.
	 */
	close() {
		let self = this;
		this.addReadyListener(function() {
			self._dispatchStateChangeEvent('closing');
		});
	}

	_addStateChangeListener(listenerState, listener) {
		let listenerStateOrd = BaseMain.STATE[listenerState.toUpperCase()];

		if (this.state > listenerStateOrd) { // previous state, go away.
			throw new Error('Registering listener for state ' + listenerState + ' after it\'s end.');
		} else if (this.state === listenerStateOrd) { // current state, execute.
			listener();
		} else { // upcoming state, wait until we get there.
			return this._listeners[listenerState].push(listener);
		}
	}

	/**
	 * Registers a listener to trigger once the application enters the "closing" state.
	 * Use this event to close tasks, clean up and save before the process exits.
	 * @param {Function} listener The listener, may return a promise to wait for.
	 */
	addCloseListener(listener) {
		return this._addStateChangeListener('closing', listener);
	}

	/**
	 * Registers a listener to trigger once the application enters the "ready" state.
	 * Use this event to execute code once the application is up and running, like tests.
	 * @param {Function} listener The listener, may return a promise to wait for.
	 */
	addReadyListener(listener) {
		this._addStateChangeListener('ready', listener);
	}

	/**
	 * Registers a listener to trigger once the application enters the "post-init" state.
	 * Use this event to init tasks, like a web server or child processes.
	 * @param {Function} listener The listener, may return a promise to wait for.
	 */
	addPostInitListener(listener) {
		this._addStateChangeListener('postinit', listener);
	}

	/**
	 * Registers a listener to trigger once the application enters the "init" state.
	 * Use this event to configure your application.
	 * @param {Function} listener The listener, may return a promise to wait for.
	 */
	addInitListener(listener) {
		this._addStateChangeListener('init', listener);
	}

	/**
	 * Registers a listener to trigger once the application enters the "pre-init" state.
	 * @param {Function} listener The listener, may return a promise to wait for.
	 */
	_addPreInitListener(listener) {
		this._addStateChangeListener('preinit', listener);
	}

	_dispatchStateChangeEvent(state) {
		this.appContext.appLogger.info(`Application state is now "${state}".`);
		let listeners = this._listeners[state];

		let promises = [];
		for (let listener of listeners) {
			let result = listener(this);

			if (result && result.then !== void 0) {
				promises.push(result);
			}
		}

		delete this._listeners[state];

		return Promise.all(promises);
	}
}

module.exports = BaseMain;
module.exports.STATE = {
	BOOT: 0,
	PREINIT: 1,
	INIT: 2,
	POSTINIT: 3,
	READY: 4,
	CLOSING: 5
};