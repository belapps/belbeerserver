'use strict';

const BaseDescriptor = require('../BaseDescriptor');
const Errors = require('../../core/Errors');
const StringUtil = require('../../util/StringUtils');
const BaseServer = require('../BaseServer');

const _ = require('underscore');
const arrayTools = require('array-tools');

class RestApiPrinter extends BaseDescriptor {

	/** @param {!JsDocCache} jsDocParser */
	constructor(jsDocParser) {
		super(jsDocParser);

		this._jsDocParser = jsDocParser;
		this._customTypes = [];
	}

	printMdDocumentation(callback) {
		const self = this;

		this.addReadyListener(function() {
			const routes = self.routes;

			_.sortBy(routes, function(o) { return o.path; });

			let document = '## Route Documentation';
			for (let i = 0; i < routes.length; i++) {
				let route = routes[i];

				let title = StringUtil.capitalize(StringUtil.camelCaseToSnakeCase(route.doc.name).replace(/_/g, ' '));

				document += `
### ${title}

\`${route.method} ${route.path}\`
`;
				if (route.authType === BaseServer.AUTH_TYPE.HARD) {
					document += 'Requires authentication\n';
				}

				document += '\n' + route.doc.description + '\n';

				document += self.printParams(route.doc.params, 'pathparameter', 'Path parameters');
				document += self.printParams(route.doc.params, 'queryparameter', 'Query parameters');
				document += self.printParams(route.doc.params, 'bodyparameter', 'Body parameters');
				document += self.printParams(route.doc.params, 'headerparameter', 'Header parameters');

				if (route.doc.errnos !== void 0) {
					document += '\nPossible errnos:\n\n';

					for (let j = 0; j < route.doc.errnos.length; j++) {
						let errno = route.doc.errnos[j];

						document += '* `' + errno.num + '`: ' + errno.message + '\n';
					}
				}

				document += '\nResponse';
				if (route.doc.response === void 0) {
					document += ': none\n';
				} else {
					if (route.doc.response.type !== void 0)
						document += ' (' + self.typesToString(route.doc.response.type) + ')';

					document += ': ';
					if (route.doc.response.description !== void 0)
						document += route.doc.response.description;
					else
						document += 'No details.';

					if (route.doc.response.nullable === true) {
						document += ' Can be NULL';
					}

					document += '\n';
				}
			}

			document += '\n## Type documentation\n';

			for (let i = 0; i < self._customTypes.length; i++) {
				let type = self._customTypes[i];

				document += '\n### ' + type + '\n';
				document += self.printCustomType(type) + '\n';
			}

			callback(document);
		});
	}

	typesToString(typeArray) {
		if (!(typeArray instanceof Array)) {
			if (typeArray.names !== void 0) {
				return this.typesToString(typeArray.names);
			}

			console.log(typeArray);
			throw new Error('Unknown data ' + JSON.stringify(typeArray));
		}

		let result = [];

		for (let i = 0; i < typeArray.length; i++) {
			result.push(this.typeToString(typeArray[i]));
		}

		return result.join(', ');
	}

	typeToString(type) {
		// array
		let arrayTypes = type.match(/Array\.<([^>]+)>/m);
		let isArrayType = arrayTypes !== null;
		if (isArrayType) {
			type = arrayTypes[1];

			return `${this.typeToString(type)}[]`;
		}

		// custom types:
		if (this._jsDocParser.getTypeDefinition(type) !== void 0) {
			if (!_.contains(this._customTypes, type)) {
				this._customTypes.push(type);
			}

			return '['+type+'](#markdown-header-' + type + ')';
		}

		// others
		return type;
	}

	printCustomType(type) {
		let typedef = this._jsDocParser.getTypeDefinition(type);

		if (typedef === void 0) {
			console.log('Unknown type !', type);
			return 'Unknown type ' + type;
		}

		let documentation = typedef.properties;
		let properties = {};

		if (documentation !== void 0) {
			console.log(documentation);
			for (let i = 0; i < documentation.length; i++) {
				let property = documentation[i];

				let propertyNames = [];
				if (property.nullable !== false) {
					propertyNames.push('nullable');
				}

				if (property.optional === true) {
					propertyNames.push('optional');
				}

				properties[property.name] = property.description + ' ' + (propertyNames.length === 0 ? '' : '(' + propertyNames.join(', ') + ') ') + this.typesToString(property.type);
			}
		}

		return ('\n' + JSON.stringify(properties, null, '  ')).replace(/\n/g, '\n\t');
	}

	printParam(param) {
		let document = '';
		document += '* `' + param.name + '`';

		if (param.type !== void 0) {
			document += '(' + this.typesToString(param.type) + '):';
		}

		document += ' ' + param.description;

		if (param.nullable === true) {
			document += ' Nullable.';
		}

		if (param.optional === true) {
			document += ' Optional.';
		}

		return document;
	}

	printParams(paramsList, type, title) {
		let filteredParams = arrayTools.where(paramsList, { paramType: type });

		let document = '';
		if (filteredParams.length !== 0) {
			document += '\n' + title + ':\n\n';

			for (let param of filteredParams) {
				document += this.printParam(param);
				document += '\n';
			}
		}

		return document;
	}
}

module.exports = RestApiPrinter;