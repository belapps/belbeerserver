'use strict';

class BaseServer {
	setAuthMethod() {}

	registerSoftAuthRoute(method, route, routeHandler, router) {
		return this.registerRoute(method, route, routeHandler, router, { authType: BaseServer.AUTH_TYPE.SOFT });
	}

	registerAuthRoute(method, route, routeHandler, router) {
		return this.registerRoute(method, route, routeHandler, router, { authType: BaseServer.AUTH_TYPE.HARD });
	}

	registerRoute(method, route, routeHandler, router, metadata) {

	}

	listen(port) {}

	close() {}
}

BaseServer.AUTH_TYPE = {
	NONE: 0,
	SOFT: 1,
	HARD: 2
};

module.exports = BaseServer;