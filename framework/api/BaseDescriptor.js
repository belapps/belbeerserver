'use strict';

class BaseDescriptor {

	/**
	 * @param {!JsDocCache} jsDocParser
	 */
	constructor(jsDocParser) {
		/** @type {!RouteDocumentation[]}  */
		this._routes = [];
		this._apiBasePath = '/';
		this._jsDocParser = jsDocParser;

		this._waiting = 0;
		this._callbacks = [];
	}

	/**
	 * @param {!string} path
	 */
	set basePath(path) {
		this._apiBasePath = path;
	}

	registerRoute(method, route, router, routeHandler, metadata) {
		this._waiting++;

		const self = this;

		metadata = metadata || { requiresLogged: false };

		// replace :params by {params}
		metadata.path = route.replace(/:([^\/]*)/, '{$1}');
		metadata.method = method;

		let methodName = routeHandler.name.startsWith('bound ') ? routeHandler.name.substring(6) : routeHandler.name;

		this._jsDocParser.getDocumentation(router.constructor.name, methodName, function(error, methodDoc) {
			if (error) {
				console.log(error);
				return;
			}

			if (methodDoc !== void 0) {
				delete methodDoc.id;
				delete methodDoc.kind;
				delete methodDoc.scope;
				delete methodDoc.longname;
				delete methodDoc.order;
				delete methodDoc.memberof;
				delete methodDoc.comment;
				delete methodDoc.meta;
				delete methodDoc['___id'];
				delete methodDoc['___s'];

				metadata.doc = methodDoc;
				self._routes.push(metadata);
			}

			if (methodDoc === void 0) {
				console.error('No documentation available for', method, route);
			}

			self._waiting--;
			if (self._waiting === 0) {
				for (let i = 0; i < self._callbacks.length; i++) {
					self._callbacks[i]();
				}

				self._callbacks = [];
			}
		});
	}

	addReadyListener(callback) {
		if (this._waiting === 0) {
			return callback();
		}

		this._callbacks.push(callback);
	}

	get routes() {
		return this._routes;
	}
}

/**
 * @typedef {Object} RouteDocumentation
 * @property {!Number} id			The user's id.
 * @property {!String} username		The user's username.
 * @property {!String} email		The user's email.
 * @property {!String} rank			The user's email.
 */

module.exports = BaseDescriptor;