'use strict';

let BaseDescriptor = require('../BaseDescriptor');

class RestApiDescriptor extends BaseDescriptor {

	/** @param {!JsDocCache} jsDocParser */
	constructor(jsDocParser) {
		super(jsDocParser);
	}

	apiRoute(request, response) {
		response.status(404).send('NYI');

		// TODO wadl documentation
	}
}

module.exports = RestApiDescriptor;