'use strict';

const Errno = require('../Errno');
const Errors = require('../../core/Errors');
const HttpUtils = require('../../util/HttpUtils');
const BaseServer = require('../BaseServer');

const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('node-uuid');
const fs = require('fs');

class HttpServer extends BaseServer {

	/**
	 * @param {!AppContext} appContext
	 * @param {!BaseDescriptor} restApiDescriptor
	 */
	constructor(appContext, restApiDescriptor) {
		super();
		const moduleName = this.constructor.MODULE_NAME;
		this.log = appContext.makeLogger(this);

		let expressServer = express();
		this.expressServer = expressServer;
		this.appContext = appContext;
		this.restApiDescriptor = restApiDescriptor;

		/**
		 * Prints out the REST API WSDL documentation. http://www.w3.org/TR/wsdl
		 */
		expressServer.get('/documentation', this.restApiDescriptor.apiRoute);

		expressServer.use(function(request, response, next) {
			request.logger = appContext.appLogger.child({ request_id: uuid.v4(), module: moduleName });
			request.logger.info('ip: "' + request.ip + '", request: ' + request.method + ' ' + request.originalUrl);

			request.humanReadable = HttpUtils.shouldBeHumanReadable(request);
			if (!request.accepts('json')) {
				HttpUtils.deny(request, 406, Errno.misuse, 'This api will only return JSON. Set the "Accept" header to "application/json" (or remove it)');
				return;
			}

			const contentType = request.get('Content-Type');
			if (contentType !== void 0 && !contentType.includes('/json') && !contentType.includes('/x-www-form-urlencoded')) {
				HttpUtils.deny(request, 415, Errno.misuse, 'This api will only accept JSON or urlencoded data. Set the "Content-Type" header to either "application/json", or "*/x-www-form-urlencoded"');
				return;
			}

			request.success = function(data, httpCode) {
				if (data == null) {
					data = {};
				} else if (Array.isArray(data)) {
					data = { list: data };
				} else if (typeof data !== 'object') {
					data = { data };
				}

				httpCode = httpCode || 200;

				if (request.humanReadable) {
					data.http_code = httpCode;
					data.http_message = http.STATUS_CODES[data.http_code];

					if (typeof data.getEntityType === 'function') {
						// todo: data in data doesn't have type.
						data['_type'] = data.getEntityType();
					}
				}

				request.logger.info('Request succeeded');
				request.res.status(httpCode).json(data);
			};

			request.deny = function(error) {
				return next(error);
			};

			next();
		});

		expressServer.use(bodyParser.urlencoded({ extended: true }));
		expressServer.use(bodyParser.json());
		expressServer.use(function(error, req, res, next) { // bodyParser error handler
			if (error instanceof SyntaxError) {
				HttpUtils.deny(req, 400, Errno.misuse, 'Error while processing body: ' + error.message);
			} else {
				next();
			}
		});
	}

	setAuthMethod(method) {
		this.authentify = method;
	}

	registerSoftAuthRoute(method, route, routeHandler, router) {
		return this.registerRoute(method, route, routeHandler, router, { authType: HttpServer.AUTH_TYPE.SOFT });
	}

	registerAuthRoute(method, route, routeHandler, router) {
		return this.registerRoute(method, route, routeHandler, router, { authType: HttpServer.AUTH_TYPE.HARD });
	}

	registerRoute(method, route, routeHandler, router, metadata) {
		this.log.info('Registering route ' + method + ' ' + route);

		if (metadata === void 0) {
			metadata = {};
		}

		if (metadata.authType === void 0) {
			metadata.authType = HttpServer.AUTH_TYPE.SOFT;
		}

		try {
			let method_ = method.toLowerCase();

			if (metadata.authType > HttpServer.AUTH_TYPE.NONE) {
				let self = this;
				this.expressServer[method_](route, function(request, response, next) {
					if (self.authentify == void 0) {
						throw new Errors.BaseException('Route requires login but no login method has been registered.', Errno.unknown);
					}

					self.authentify(request, response, next, metadata.authType);
				});
			}

			this.expressServer[method_](route, routeHandler);

			this.restApiDescriptor.registerRoute(method, route, router, routeHandler, metadata);
		} catch(e) {
			this.log.error('Error while registering route ' + method + ' ' + route);

			throw e;
		}

		return this;
	}

	listen(port) {
		if (this.closed) {
			return this;
		}

		let self = this;

		/**
		 * Handles 404
		 */
		this.expressServer.use(function(req, res, next) {
			HttpUtils.deny(req, 404, Errno.misuse, 'There is no such resource');
		});

		/**
		 * Handles every error except 500 & 404.
		 */
		this.expressServer.use(function(err, req, res, next) {
			if (err.originalError !== void 0) err = err.originalError;
			if (err instanceof Errors.BaseException) {
				if (err.httpCode === 500) {
					return next(err);
				}

				HttpUtils.deny(req, err.httpCode, err.code, err.message);
				return;
			}

			let validationErrors = HttpUtils.isValidationError(err);
			if (validationErrors !== null) {
				let errnos = HttpUtils.getValidationErrnos(validationErrors);
				let messages = req.humanReadable ? HttpUtils.getValidationMessages(validationErrors) : void 0;

				HttpUtils.deny(req, 422, errnos, messages);
				return;
			}

			next(err);
		});

		/**
		 * Handles error 500
		 */
		this.expressServer.use(function(err, req, res, next) {
			req.logger.warn('Request failed');
			req.logger.warn(err);

			self.appContext.onError(err);

			HttpUtils.deny(req, 500, Errno.unknown, 'We\'re sorry :\'(');
		});

		this.nodeServer = this.expressServer.listen(port);
		this.log.info('HTTP Server running on port ' + port);

		return this;
	}

	close() {
		this.closed = true;
		if (this.nodeServer !== void 0) {
			this.log.info('HTTP Server closing');

			let self = this;
			this.nodeServer.close(function() {
				self.log.info('HTTP Server closed');
			});
		} else {
			this.log.info('HTTP Server launch cancelled');
		}
	}
}

module.exports = HttpServer;