'use strict';

const _ = require('underscore');

module.exports = {
	misuse: -2,
	unknown: -1,

	username_unique: 1,
	email_unique: 2,
	email_invalid: 3,
	insufficient_perms: 4,
	datetime_invalid: 5,
	rank_invalid: 6,
	name_invalid: 7,
	beername_invalid: 7,
	username_invalid: 7,
	user_invalid: 7,
	password_invalid: 8,
	entity_not_found: 9,
	brewery_invalid: 9,
	beer_invalid: 9,

	login_failed_username: 10,
	login_failed_password: 11,

	color_invalid: 12,
	abv_invalid: 13,
	classification_invalid: 14,
	comment_invalid: 15,
	text_invalid: 15,
	rating_invalid: 16,
	website_invalid: 17
};

const SYNONYMS = {
	invalid: ['notContains', 'intColor', 'min', 'max', 'in', 'string', 'email', 'notNull', 'required', 'integer', 'float']
};

module.exports.getErrorType = function(errorType) {
	for (let synonym in SYNONYMS) {
		let synonymList = SYNONYMS[synonym];

		if (_.contains(synonymList, errorType)) {
			return synonym;
		}
	}

	return errorType;
};

module.exports.DEFAULT_MESSAGES = [
	'Success',
	'Username is not unique',
	'Email is not unique',
	'Invalid email syntax',
	'Insufficient Permissions',
	'Invalid datetime syntax',
	'Invalid rank syntax/value',
	'Invalid name syntax',
	'Invalid password syntax',
	'Entity not found',
	'Authentication failure - Username not found',
	'Authentication failure - Password mismatch',
	'Invalid color syntax',
	'Invalid ABV syntax',
	'Invalid classification syntax'
];