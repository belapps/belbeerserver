'use strict';

const http = require('http');
const _ = require('underscore');

const Errno = require('../api/Errno');
const Errors = require('../core/Errors');

class HttpUtils {

	// TODO: move to Errnos
	static getValidationMessages(validationErrors) {
		let messages = ['Note: Waterline has a bug, attributes are sometimes named `undefined` or `null`. Refer to the doc'];

		for (let fieldName in validationErrors) {
			if (!validationErrors.hasOwnProperty(fieldName))
				continue;

			let errorList = validationErrors[fieldName];

			for (let error of errorList) {
				messages.push(error.message);
			}
		}

		return messages;
	}

	// TODO: move to Errnos
	static getValidationErrnos(validationErrors) {
		let errnos = [];

		for (let fieldName in validationErrors) {
			if (!validationErrors.hasOwnProperty(fieldName))
				continue;

			let errorList = validationErrors[fieldName];

			for (let error of errorList) {
				let errorType = Errno.getErrorType(error.rule);

				let errnoName = fieldName + '_' + errorType;

				let errno = Errno[errnoName] || Errno.unknown;

				if (errno === Errno.unknown) {
					console.log('unknown error ' + errnoName);
				}

				if (!_.contains(errnos, errno)) {
					errnos.push(errno);
				}
			}
		}

		return errnos;
	}

	// TODO: remove
	static success(request, data) {
		request.success(data);
	}

	// TODO: remove, replace by method on request
	static deny(request, http_code, errnos, messages, additionalHeaders) {

		if (errnos == null) {
			throw new Errors.IllegalArgumentException('HttpUtils::deny - "errnos" cannot be null.');
		}

		if (!(errnos instanceof Array)) {
			errnos = [errnos];
		}

		let response = {
			errnos
		};

		if (additionalHeaders) {
			for (let additionalHeader in additionalHeaders) {
				if (additionalHeaders.hasOwnProperty(additionalHeader)) {
					request.res.set(additionalHeader, additionalHeaders[additionalHeader]);
				}
			}
		}

		if (request.humanReadable) {
			if (!(messages instanceof Array)) {
				let newMsg = [];

				if (messages !== void 0) {
					newMsg.push(messages);
				} else {
					for (let i = 0; i < errnos.length; i++) {
						if (errnos[i] >= 0) {
							newMsg.push(Errno.DEFAULT_MESSAGES[errnos[i]]);
						}
					}
				}

				messages = newMsg;
			}

			response.http_code = http_code;
			response.http_message = http.STATUS_CODES[http_code];
			response.error_messages = messages;
		}

		request.logger.info('Request failed with status', http_code);
		request.res.status(http_code).json(response);

		return false;
	}

	static shouldBeHumanReadable(request) {
		return request.get('X-Human-Readable') === '1';
	}

	static isValidationError(error) {
		if (!(error instanceof Error) || error.invalidAttributes === void 0 || error.invalidAttributes.length === 0) {
			return null;
		}

		return error.invalidAttributes;
	}

	static ensureParameters(request, paramList, source, sourceName) {
		for (let i = 0; i < paramList.length; i++) {
			let param = source[paramList[i]];

			if (param === void 0) {
				return HttpUtils.deny(request, 400, Errno.misuse, 'Missing ' + sourceName + ' parameter "' + paramList[i] + '"');
			}
		}

		return true;
	}

	static ensureBodyParameters(request, paramList) {
		return HttpUtils.ensureParameters(request, paramList, request.body, 'body');
	}

	static ensurePathParameters(request, paramList) {
		return HttpUtils.ensureParameters(request, paramList, request.params, 'path');
	}

	static ensureQueryParameters(request, paramList) {
		return HttpUtils.ensureParameters(request, paramList, request.query, 'query');
	}

	static ensureLogged(request) {
		if (!request.user) {
			HttpUtils.deny(request, 401, Errno.misuse, 'You need to be logged in to access this resource.', {'WWW-Authenticate': 'JWT'});
			return false;
		}

		return true;
	}
}

module.exports = HttpUtils;