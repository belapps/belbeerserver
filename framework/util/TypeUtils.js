'use strict';

/**
 * @typedef {number} Uint32
 * @typedef {number} Int32
 * @typedef {number} Float
 */
class TypeUtils {

	static parseTimestamp(string) {
		let timestamp = Date.parse(string);
		if (Number.isNaN(timestamp)) {
			return null;
		}

		return timestamp;
	}

	static parseNumber(val) {
		return +val;
	}

	static isUnsignedInt(val) {
		return Number.isInteger(val) && val >= 0;
	}

	static isFloat(val) {
		return !isNaN(val) && !Number.isInteger(val);
	}

	static isRGB(val) {
		return TypeUtils.isUnsignedInt(val) && val <= 0xffffff;
	}

	static isEmptyString(str) {
		return str == null || str.length == null || str.length == 0;
	}
}

module.exports = TypeUtils;