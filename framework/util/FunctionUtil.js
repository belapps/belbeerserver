'use strict';

const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
const ARGUMENT_NAMES = /([^\s,]+)/g;
const _ = require('underscore');

const Errors = require('../core/Errors');

class FunctionUtil {
	static getParameters(func) {
		if (func.PARAMETERS !== void 0) return func.PARAMETERS;

		if (!(func instanceof Function)) {
			throw new Errors.IllegalArgumentException('FunctionUtil#getParameters: Requesting function parameter but given object is not a function.');
		}

		let fnStr = func.toString().replace(STRIP_COMMENTS, '');

		if (fnStr.startsWith('class ')) {

			// remove everything until "constructor..."
			let constructorPos = fnStr.indexOf('constructor');
			if (constructorPos === -1) {
				return [];
			}

			fnStr = fnStr.slice(constructorPos);
		}

		let result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);

		if (result === null)
			result = [];

		return result;
	}

	static getMethods(clazz) {
		if (clazz == null || typeof clazz !== 'object' || clazz.constructor.name === 'Function' || clazz.constructor.name === 'Object') return [];

		let parentMethods = FunctionUtil.getMethods(Object.getPrototypeOf(clazz));
		let clazzMethods = Object.getOwnPropertyNames(clazz).filter(function(methodName) {
			let method = clazz[methodName];
			return (method !== clazz.constructor && typeof method === 'function');
		});

		return _.union(clazzMethods, parentMethods);
	}

	static bindClass(clazz, bindFrom) {
		if (bindFrom === void 0) bindFrom = clazz;

		let methodNames = FunctionUtil.getMethods(bindFrom);

		for (let i = 0; i < methodNames.length; i++) {
			let methodName = methodNames[i];
			// private method, should never leave the class. No need to bind.
			if (methodName.startsWith('_')) {
				continue;
			}

			let method = clazz[methodName];

			if (method === clazz.constructor || typeof method !== 'function') continue;

			let parameters = FunctionUtil.getParameters(method);

			clazz[methodName] = method.bind(clazz);
			Object.defineProperty(clazz[methodName], 'PARAMETERS', {
				enumerable: false,
				configurable: false,
				writable: false,
				value: parameters
			});
		}

		return clazz;
	}
}

module.exports = FunctionUtil;