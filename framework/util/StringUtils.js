'use strict';

class StringUtils {
	static capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	static camelCaseToSnakeCase(name) {
		return name.replace(/([A-Z])/g, function($1) {
			return '_' + $1.toLowerCase();
		});
	}
}

module.exports = StringUtils;