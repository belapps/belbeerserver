'use strict';

/**
 * @type {!HttpServerMain}
 */
const app = require('../../app');
const Error = require('../../framework/core/Errors');

const toCleanup = {};
exports.markForCleanup = function(entity) {
	if (entity._type === void 0) {
		throw new Error.IllegalArgumentException('testBase::markForCleanup - entity has no "_type" property, undefined behavior.');
	}

	console.log('Marking ' + entity._type + ' for cleanup.');

	if (toCleanup[entity._type] === void 0) {
		toCleanup[entity._type] = [];
	}

	toCleanup[entity._type].push(entity);
};

exports.premade = {
	user: {},
	beer: {},
	brewery: {}
};

exports.ucc = {};

exports.setupTest = function(testExports) {
	const password = 'chocolatine';

	testExports.setUp = function(callback) {
		Promise.all([
			app.injector.create('UserUcc').then(/** @param {!UserUcc} userUcc */ function(userUcc) {
				exports.ucc.user = userUcc;

				return Promise.all([
					userUcc.create('admin_dude', 'admin@app.net', password, 1),
					userUcc.create('regular_user', 'regular@app.net', password, 0)
				]);
			}).then(function(users) {
				for (let user of users) {
					user.password = password;
				}

				exports.premade.user.admin = users[0];
				exports.premade.user.regular = users[1];
			}),

			app.injector.create('BreweryUcc').then(/** @param {!BreweryUcc} breweryUcc */ function(breweryUcc) {
				exports.ucc.brewery = breweryUcc;

				return Promise.all([
					breweryUcc.create('alpha brewery', 'http://brewery.fr', 'brewery@mail.net', '010229709', 'Wallaby Way', '42', 'Australia', 'Sydney', '2000'),
					breweryUcc.create('beta brewery', 'http://brewery.fr', 'brewery@mail.net', '010229709', 'Wallaby Way', '42', 'Australia', 'Sydney', '2000')
				]);
			}).then(function(brewery) {
				exports.premade.brewery.alpha = brewery[0];
				exports.premade.brewery.beta = brewery[1];

				return app.injector.create('BeerUcc');
			}).then(/** @param {!BeerUcc} beerUcc */ function(beerUcc) {
				exports.ucc.beer = beerUcc;

				return beerUcc.create('Chimay Rouge', 0xff0000, 0.6, 1, exports.premade.brewery.alpha.id);
			}).then(function(beer) {
				exports.premade.beer.chimay = beer;
			})
		]).then(function() {
			callback();
		}).catch(callback);
	};

	testExports.tearDown = function(callback) {
		cleanup(['user', 'beer', 'brewery']).then(function() {
			callback();
		}).catch(function(err) {
			console.error(err);
			callback(err);
		});
	};
};

function cleanup(lists) {
	let promises = [];

	for (let list of lists) {
		let ucc = exports.ucc[list];

		let premade = exports.premade[list];
		if (premade !== void 0) {
			for (let i in premade) {
				promises.push(ucc.destroy(premade[i].id));

				delete premade[i];
			}
		}

		let handmade = toCleanup[list];
		if (handmade !== void 0) {
			for (let entity of handmade) {
				promises.push(ucc.destroy(entity.id));
			}

			handmade.length = 0;
		}
	}

	return Promise.all(promises);
}
