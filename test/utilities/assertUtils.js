'use strict';

const app = require('../../app');
const request = require('request-promise');
const TestBase = require('./testBase');
const _ = require('underscore');

const encodeBase64 = exports.encodeBase64 = function(str) {
	return new Buffer(str).toString('base64')
};

function getAuthToken(username, password) {
	console.log('\nAUTH > Authenticating ' + username);

	return request({
		uri: 'http://localhost:' + (app.getCliOption('P') || 3000) + '/login',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			'X-Human-Readable': 1,
			'Authorization': 'Basic ' + encodeBase64(username + ':' + password)
		},
		json: true
	}).then(function(data) {
		console.log('AUTH > Token acquired for ' + username);

		return data.token;
	}, function(error) {
		console.log('AUTH > Couldn\'t acquire token for ' + username);
		console.error(error);
	});
}

exports.makeRequest = function(method, path, body, auth) {
	function actualRequest(token) {
		body = body || '';

		return request({
			uri: 'http://localhost:' + (app.getCliOption('P') || 3000) + path,
			method,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'X-Human-Readable': 1,
				'Authorization': token ? ('JWT ' + token) : ''
			},
			body,
			json: true,
			resolveWithFullResponse: true,
			simple: false
		});
	}

	if (auth) {
		return getAuthToken(auth.username, auth.password).then(actualRequest);
	}

	return actualRequest();
};

/**
 * Creates a test function to test that a given route requires authentication.
 * @param {!string} httpMethod The http method to use for this route (GET, POST, PUT, PATCH, DELETE,..).
 * @param {!string} path       The route path.
 * @return {!Function} The test function.
 */
exports.buildTestRequiresLogin = function(httpMethod, path) {
	return function(test) {
		exports.testRequest(test, {
			request: exports.makeRequest(httpMethod, path),
			httpCode: 401,
			errnos: [-2],
			final: true
		});
	};
};

/**
 * Test several parts of an HTTP request based on expected values.
 * @param                  test             Unit test tool.
 * @param {!Object}        params           The parameters for this test.
 * @param {!Promise}       params.request   The http request handler as a promise.
 * @param {number=200}     params.httpCode  The http code to expect as a response for the request.
 * @param {number[]=null}       params.errnos    The list of errnos to expect as a response for the request.
 * @param {boolean=false}  params.final     End the test directly instead of returning a promise.
 * @return {Promise|undefined}
 */
exports.testRequest = function(test, params) {
	let request = params.request;
	let httpCode = params.httpCode || 200;
	let errnos = params.errnos || [];
	let bFinal = params.final === void 0 ? false : params.final;

	let promise = request.then(function(response) {
		if (response.statusCode === 201) {
			TestBase.markForCleanup(response.body);
		}

		if (!_checkResponsePromise(test, httpCode, errnos, response.statusCode, response.body.errnos)) {
			throw new Error('Request didn\'t pass, debug body data: ' + JSON.stringify(response.body));
		}

		if (bFinal) {
			test.done();
			return;
		}

		return response.body;
	}).catch(function(error) {
		test.ok(false, error);
		test.done();
	});

	if (!bFinal) return promise;
};

exports.entityEqual = function(test, entity1, entity2, message) {
	delete entity1._type;
	delete entity1.lastUpdate;

	delete entity2._type;
	delete entity2.lastUpdate;

	test.ok(exports.objectEqual(test, entity1, entity2), message + ' | Objects are not identical: ' + JSON.stringify(entity1) + ' - ' + JSON.stringify(entity2));
};

exports.simNetworkOnObject = function(obj) {
	return JSON.parse(JSON.stringify(obj));
};

exports.objectEqual = function(test, object1, object2, prefix) {
	prefix = prefix || '';

	let firstIsObj = typeof object1 === 'object';
	let secondIsObj = typeof object2 === 'object';

	test.ok(firstIsObj, prefix + ' First object is not an object, it is: ' + JSON.stringify(object1) + ' (type: '+ typeof object1 +')' + '. Second object is: ' + JSON.stringify(object2) + ' (type: '+ typeof object2 +')');
	test.ok(secondIsObj, prefix + ' Second object is not an object, it is: ' + JSON.stringify(object2) + ' (type: '+ typeof object2 +')' + '. First object is: ' + JSON.stringify(object1) + ' (type: '+ typeof object1 +')');

	if (!firstIsObj || !secondIsObj)
		return false;

	for (let i in object1) {
		if (!i in object2) {
			test.ok(false, 'Key ' + (prefix + i) + ' is in the first object but not in the second.');
			return false;
		}

		if (typeof object1[i] === 'object' || typeof object2[i] === 'object') {
			if (!exports.objectEqual(test, object1[i], object2[i], prefix + i + '.')) {
				return false;
			}
		} else {
			test.strictEqual(object1[i], object2[i], 'Key "' + (prefix + i) + '" does not have the same value in both objects. (' + object1[i] + ', ' + object2[i] + ')');
		}
	}

	return true;
};

function _checkResponsePromise(test, expectedHttpCode, expectedErrnos, actualHttpCode, actualErrnos) {
	return _testHttpCode(test, actualHttpCode || 200, expectedHttpCode) && _testErrnos(test, actualErrnos || [], expectedErrnos);
}

function _testErrnos(test, actualErrnos, requestedErrnos) {
	let notFoundErrnos = _.difference(requestedErrnos, actualErrnos);
	let unhandledErrnos = _.difference(actualErrnos, requestedErrnos);

	test.equal(0, notFoundErrnos.length, 'HTTP request did not send the following expected errnos: ' + JSON.stringify(notFoundErrnos));
	test.equal(0, unhandledErrnos.length, 'HTTP request contains unexpected errnos: ' + JSON.stringify(unhandledErrnos));

	return notFoundErrnos.length === 0 && unhandledErrnos.length === 0;
}

function _testHttpCode(test, param2, expectedCode) {
	let error, actualCode;
	if (typeof param2 === 'object') {
		error = param2;
		actualCode = param2.statusCode === void 0 ? param2.error : param2.statusCode;
	} else {
		error = null;
		actualCode = param2;
	}

	test.equal(expectedCode, actualCode, 'HTTP status code should be ' + expectedCode + ', got ' + actualCode + '. ' + (error == null || error.body == null ? '' : JSON.stringify(error.response.body)));

	return actualCode === expectedCode;
}