'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

const DEFAULT_USER = {
	username: 'john',
	password: 'boule_de_neige',
	email: 'john@it.net',
	rank: 0
};

function makeUser(userData) {
	if (userData === void 0) return DEFAULT_USER;

	if (userData.username === void 0) {
		userData.username = DEFAULT_USER.username;
	}

	if (userData.password === void 0) {
		userData.password = DEFAULT_USER.password;
	}

	if (userData.email === void 0) {
		userData.email = DEFAULT_USER.email;
	}

	if (userData.rank === void 0) {
		userData.rank = DEFAULT_USER.rank;
	}

	return userData;
}

const userTest = {};
TestBase.setupTest(userTest);

userTest.testCreateUser_InvalidSyntax = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/account', makeUser({
			username: 'john:smith',
			email: 'patate',
			password: ''
		})),

		httpCode: 422,
		errnos: [Errno.email_invalid, Errno.username_invalid, Errno.password_invalid],
		final: true
	});
};

userTest.testCreateUser_Null = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/account', makeUser({
			username: null,
			email: null,
			password: null
		})),

		httpCode: 422,
		errnos: [Errno.email_invalid, Errno.username_invalid, Errno.password_invalid],
		final: true
	});
};

userTest.testCreateUser = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/account', makeUser({
			rank: 1
		})),

		httpCode: 201,
		final: false
	}).then(function(user) {
		test.strictEqual(DEFAULT_USER.username, user.username);
		test.strictEqual(DEFAULT_USER.email, user.email);
		test.strictEqual(void 0, user.password);
		test.strictEqual(0, user.rank);
		test.notEqual(void 0, user.id);

		test.done();
	});
};

userTest.testCreateUser_DuplicateUsername = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/account', makeUser({
			username: TestBase.premade.user.regular.username
		})),

		httpCode: 422,
		errnos: [Errno.username_unique],
		final: true
	});
};

userTest.testCreateUser_DuplicateEmail = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/account', makeUser({
			email: TestBase.premade.user.regular.email
		})),

		httpCode: 422,
		errnos: [Errno.email_unique],
		final: true
	});
};

module.exports = userTest;