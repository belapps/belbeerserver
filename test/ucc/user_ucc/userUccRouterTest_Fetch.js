'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

TestBase.setupTest(module.exports);

// USER GET TEST
module.exports.testGetUser_NotExists = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/account/' + Number.MAX_SAFE_INTEGER),
		httpCode: 404,
		errnos: [Errno.entity_not_found],
		final: true
	});
};

module.exports.testGetUser_UnLogged = function(test) {
	const user = TestBase.premade.user.regular;

	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/account/' + user.id),
		httpCode: 200
	}).then(function(newUser) {
		test.equal(user.id, newUser.id, 'ID not equal');
		test.equal(user.rank, newUser.rank, 'Rank not equal');
		test.equal(user.username, newUser.username, 'Username not equal');

		test.equal(void 0, newUser.password, 'Password should not be returned.');
		test.equal(null, newUser.email, 'Email should not be returned for other accounts than our own.');

		test.done();
	});
};

module.exports.testGetUser_Logged_Regular = function(test) {
	const user = TestBase.premade.user.regular;

	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/account/' + user.id, '', user),
		httpCode: 200
	}).then(function(newUser) {
		test.equal(void 0, newUser.password, 'Password should not be returned.');
		test.equal(user.email, newUser.email, 'Email should be returned for our own account.');

		test.done();
	});
};

module.exports.testGetUser_Logged_Admin = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/account/' + user.id, '', admin),
		httpCode: 200
	}).then(function(newUser) {
		test.equal(void 0, newUser.password, 'Password should not be returned.');
		test.equal(user.email, newUser.email, 'Emails should be returned if we are an admin.');

		test.done();
	});
};

module.exports.testGetUser_UnLogged_Self = Util.buildTestRequiresLogin('GET', '/account');

module.exports.testGetUser_Logged_Self = function(test) {
	const user = TestBase.premade.user.regular;

	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/account/', '', user),
		httpCode: 200,
		final: true
	});
};