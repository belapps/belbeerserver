'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

TestBase.setupTest(module.exports);

module.exports.testUpdateUser_Unlogged = Util.buildTestRequiresLogin('PATCH', '/account/1');

module.exports.testUpdateUser_Valid = function(test) {
	const admin = TestBase.premade.user.admin;

	const NEW_USER = {
		username: 'fred',
		email: 'fred@mail.net',
		password: 'test',
		rank: 1
	};

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + admin.id, NEW_USER, admin),
		httpCode: 200
	}).then(function(newUser) {
		test.equal(newUser.username, NEW_USER.username, 'Username not modified');
		test.equal(newUser.email, NEW_USER.email, 'email not modified');
		test.done();
	});
};

module.exports.testUpdateUser_InvalidSyntax = function(test) {
	const admin = TestBase.premade.user.admin;
	const NEW_USER = {
		username: 'fred:',
		email: 'fred_mail',
		password: '',
		rank: 54
	};

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + admin.id, NEW_USER, admin),
		httpCode: 422,
		errnos: [3, 7, 8, 6],
		final: true
	});
};

module.exports.testUpdateUser_DuplicateUsername = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + user.id, {
			username: admin.username
		}, user),
		httpCode: 422,
		errnos: [1],
		final: true
	});
};

module.exports.testUpdateUser_DuplicateEmail = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + user.id, {
			email: admin.email
		}, user),
		httpCode: 422,
		errnos: [2],
		final: true
	});
};

module.exports.testUpdateUser_Regular_OtherAccount = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + admin.id, {
			username: 'test'
		}, user),
		httpCode: 403,
		errnos: [4],
		final: true
	});
};

module.exports.testUpdateUser_Admin_OtherAccount = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + user.id, {
			username: 'test'
		}, admin),
		httpCode: 200,
		final: true
	});
};

module.exports.testUpdateUser_Regular_Rank = function(test) {
	const user = TestBase.premade.user.regular;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + user.id, {
			rank: 1
		}, user),
		httpCode: 403,
		errnos: [4],
		final: true
	});
};

module.exports.testUpdateUser_Admin_Rank = function(test) {
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + admin.id, {
			rank: 0
		}, admin),
		httpCode: 200,
		final: true
	});
};

module.exports.testUpdateUser_Admin_Rank = function(test) {
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/' + admin.id, {
			rank: 0
		}, admin),
		httpCode: 200,
		final: true
	});
};

module.exports.testUpdateUser_NotExists = function(test) {
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/account/11235488', {
			rank: 0
		}, admin),
		httpCode: 404,
		errnos: [9],
		final: true
	});
};