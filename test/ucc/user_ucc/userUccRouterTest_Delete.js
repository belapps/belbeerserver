'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

TestBase.setupTest(module.exports);

module.exports.testUpdateUser_Unlogged = Util.buildTestRequiresLogin('PATCH', '/account/1');

module.exports.testDeleteUser_Self = function(test) {
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/account/' + admin.id, null, admin),
		httpCode: 200,
		final: true
	});
};

module.exports.testDeleteUser_Regular_OtherAccount = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/account/' + admin.id, null, user),
		httpCode: 403,
		errnos: [4],
		final: true
	});
};

module.exports.testDeleteUser_Admin_OtherAccount = function(test) {
	const user = TestBase.premade.user.regular;
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/account/' + user.id, null, admin),
		httpCode: 200,
		final: true
	});
};

module.exports.testUpdateUser_NotExists = function(test) {
	const admin = TestBase.premade.user.admin;

	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/account/11235488', null, admin),
		httpCode: 404,
		errnos: [9],
		final: true
	});
};