'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

const userTest = {};
TestBase.setupTest(userTest);

userTest.testUpdateBeer_InvalidSyntax = function(test) {
	const beer = TestBase.premade.beer.chimay;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/beer/' + beer.id, {
			name: '',
			color: -5,
			abv: 2,
			classification: -7,
			brewery: TestBase.premade.brewery.alpha.id
		}, TestBase.premade.user.admin),

		httpCode: 422,
		errnos: [Errno.name_invalid, Errno.color_invalid, Errno.abv_invalid, Errno.classification_invalid],
		final: true
	});
};

userTest.testUpdateBeer_WrongBrewery = function(test) {
	const beer = TestBase.premade.beer.chimay;

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/beer/' + beer.id, {
			name: 'Omega Beer',
			color: 0xffffff,
			abv: 1,
			classification: 0,
			brewery: 458
		}, TestBase.premade.user.admin),

		httpCode: 422,
		errnos: [Errno.brewery_invalid],
		final: true
	});
};

userTest.testUpdateBeer_Fine = function(test) {
	const beer = TestBase.premade.beer.chimay;

	const originalBeer =  {
		name: 'Beta Beer',
		color: 0xffff00,
		abv: 0.4,
		classification: 1,
		brewery: TestBase.premade.brewery.beta.id
	};

	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/beer/' + beer.id, originalBeer, TestBase.premade.user.admin)
	}).then(function(updatedBeer) {
		test.equal(originalBeer.name, updatedBeer.name, 'Name different');
		test.equal(originalBeer.color, updatedBeer.color, 'Color different');
		test.equal(originalBeer.abv, updatedBeer.abv, 'ABV different');
		test.equal(originalBeer.classification, updatedBeer.classification, 'Classification different');
		test.equal(originalBeer.brewery, updatedBeer.brewery, 'Brewery different');
		test.done();
	});
};

userTest.testUpdateBeer_RegularUser = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('PATCH', '/beer/4587', {}, TestBase.premade.user.regular),

		httpCode: 403,
		errnos: [Errno.insufficient_perms],
		final: true
	});
};

module.exports = userTest;