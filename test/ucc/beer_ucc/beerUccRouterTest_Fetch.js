'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

const userTest = {};
TestBase.setupTest(userTest);

userTest.testFetchBeers = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('GET', '/beer', null)
	}).then(function(data) {
		let beers = data.list;

		test.equal(1, beers.length, 'GET /beer didn\'t return an array of 1 beer, got ' + JSON.stringify(data));
		Util.entityEqual(test, Util.simNetworkOnObject(TestBase.premade.beer.chimay), beers[0], 'Returned beer did not match the persisted one.');
		test.done();
	});
};

module.exports = userTest;