'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

const userTest = {};
TestBase.setupTest(userTest);

userTest.testCreateBeer_InvalidSyntax = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/beer', {
			name: '',
			color: -5,
			abv: 2,
			classification: -7,
			brewery: TestBase.premade.brewery.alpha.id
		}, TestBase.premade.user.admin),

		httpCode: 422,
		errnos: [Errno.name_invalid, Errno.color_invalid, Errno.abv_invalid, Errno.classification_invalid],
		final: true
	});
};

userTest.testCreateBeer_WrongBrewery = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/beer', {
			name: 'Omega Beer',
			color: 0xffffff,
			abv: 1,
			classification: 0,
			brewery: 458
		}, TestBase.premade.user.admin),

		httpCode: 422,
		errnos: [Errno.brewery_invalid],
		final: true
	});
};

userTest.testCreateBeer_Fine = function(test) {
	const originalBeer =  {
		name: 'Omega Beer',
		color: 0xffffff,
		abv: 1,
		classification: 0,
		brewery: TestBase.premade.brewery.alpha.id
	};

	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/beer', originalBeer, TestBase.premade.user.admin),
		httpCode: 201
	}).then(function(createdBeer) {
		test.strictEqual(originalBeer.name, createdBeer.name, 'Name different');
		test.strictEqual(originalBeer.color, createdBeer.color, 'Color different');
		test.strictEqual(originalBeer.abv, createdBeer.abv, 'ABV different');
		test.strictEqual(originalBeer.classification, createdBeer.classification, 'Classification different');
		test.strictEqual(originalBeer.brewery, createdBeer.brewery, 'Brewery different');
		test.done();
	});
};

userTest.testCreateBeer_RegularUser = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('POST', '/beer', {}, TestBase.premade.user.regular),

		httpCode: 403,
		errnos: [Errno.insufficient_perms],
		final: true
	});
};

module.exports = userTest;