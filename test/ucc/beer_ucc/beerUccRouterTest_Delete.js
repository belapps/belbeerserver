'use strict';

const Errno = require('../../../framework/api/Errno');
const Util = require('../../utilities/assertUtils');
const TestBase = require('../../utilities/testBase');

const userTest = {};
TestBase.setupTest(userTest);

userTest.testDeleteBeer_NotFound = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/beer/4854', '', TestBase.premade.user.admin),
		httpCode: 404,
		errnos: [Errno.entity_not_found],
		final: true
	});
};

userTest.testDeleteBeer_Admin = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/beer/' + TestBase.premade.beer.chimay.id, '', TestBase.premade.user.admin),
		final: true
	});
};

userTest.testDeleteBeer_Regular = function(test) {
	Util.testRequest(test, {
		request: Util.makeRequest('DELETE', '/beer/455', {}, TestBase.premade.user.regular),
		httpCode: 403,
		errnos: [Errno.insufficient_perms],
		final: true
	});
};

module.exports = userTest;