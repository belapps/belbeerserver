'use strict';

const fs = require('fs');
const promisify = require('es6-promisify');
const reporter = require('nodeunit').reporters.default;

const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

function getUnitTests(dir, testList) {
	testList = testList || [];

	return stat(dir).then(function(stats) {
		if (stats.isFile()) {
			testList.push(dir);
			return testList;
		}

		if (stats.isDirectory()) {
			return readdir(dir).then(function(fsEntries) {
				let promises = [];

				for (let fsEntry of fsEntries) {
					fsEntry = dir + '/' + fsEntry;

					promises.push(getUnitTests(fsEntry, testList));
				}

				return Promise.all(promises).then(function() {
					return testList;
				})
			});
		}
	});
}

const app = require('../app');
app.addReadyListener(function() {
	getUnitTests(__dirname+'/ucc').then(function(data) {
		// TODO: add filter ?
		reporter.run(data, null, function() {
			console.log('Testing complete. Goodbye Caroline.');
			app.close();
		});
	});
});